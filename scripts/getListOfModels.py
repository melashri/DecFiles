#!/usr/bin/env python

## This is Sim09 compatible version, for Sim10 I will need to get list from HepForge

import json
import urllib.request
import base64

base_url = 'https://gitlab.cern.ch/api/v4/projects/2606/repository/files/Gen%2FEvtGen%2Fsrc%2F{0}.cpp?ref=Sim09'
url = base_url.format('EvtModelReg')

def getModelName(source):
  inGetName = False
  for line in source:
    if 'getName' in line:
      inGetName = True
    if inGetName and '"' in line:
      ss = line.split('"')[1]
      return ss
  return None

handle = urllib.request.urlopen(url)
sourcecode = handle.read().decode()
result = json.loads(sourcecode)
fileContent = str(base64.b64decode(result['content']) ).split('\\n')
#print(fileContent)
modelClasses = []
for line in fileContent:
  if 'registerModel' in line:
    ss = line.split('new')
    if len(ss)<2:
      continue
    mm = ss[1].split(')')[0].strip()
    modelClasses.append(mm)
#print(modelClasses)
modelNames = []
for model in modelClasses:
#  print(model)
  try:
    url = base_url.format(model)
    handle = urllib.request.urlopen(url)
    sourcecode = handle.read().decode()
    result = json.loads(sourcecode)
    fileContent = str(base64.b64decode(result['content']) ).split('\\n')
    modelNames.append(getModelName(fileContent))
  except:
    pass

result = 'terminators = ['
for model in modelNames:
  if model==None:
    continue
  result += '"{0}", '.format(model)
result += ']'
print(result)

#print(modelNames)

