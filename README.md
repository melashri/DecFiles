# Deadline for next release

The release plan with deadline for DecFiles over coming months is:


  1. DecFiles v30r76 with deadline on Friday 8th July 2022 at 14:00.
  1. DecFiles v30r77 with deadline on Friday 22nd July 2022 at 14:00.

Usually plan is to release on Monday or Tuesday after deadline, but can change depending on time availability. Merge requests created after deadline are not guaranteed to be accepted for this release.

# Guide for submitting decay file
Please follow [Contribution guide](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/blob/master/CONTRIBUTING.md) on top of the page for instructions on how to prepare, test and commit decay file.

# Overiew of existing decay files
A list of existing decay files, event numbers, and related comments can be found here: [http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/](http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/).
