# Powheg options for ZZ production
from Configurables import Generation
from Gaudi.Configuration import *

Generation().PileUpTool = "FixedLuminosityForRareProcess"

importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )

from Configurables import Special, PowhegProduction

Generation().addTool( Special )
Generation().Special.addTool( PowhegProduction )

# Powheg options.
Generation().Special.addTool(PowhegProduction)
Generation().Special.PowhegProduction.Process = "Z"
Generation().Special.PowhegProduction.Commands += [
    "lhans1 10770", # Change the first proton PDF.
    "lhans2 10770", # Change the second proton PDF.
    "vdecaymode 1", # decay mode Z-> e e
    "mass_low  40",
]

