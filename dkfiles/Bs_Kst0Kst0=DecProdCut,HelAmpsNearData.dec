# EventType: 13104004
#
# Descriptor: [B_s0 -> (K*(892)0 -> K+ pi-) (K*(892)~0 -> K- pi+)]cc
#
# NickName: Bs_Kst0Kst0=DecProdCut,HelAmpsNearData
#
# Cuts: DaughtersInLHCb
#
# Documentation:
# B_s0 decaying into two vectors K*(892)0 and K*(892)~0.
# K*(892)0 and K*(892)~0 decaying into (K+ pi-) and (K- pi+), respectively.
# The helicity amplitudes are chosen from measured data (https://arxiv.org/abs/1712.08683)
# Daughters in acceptance.
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Asier Pereiro
# Email: asier.pereiro.castro@cern.ch
# Date: 20201119
#
Define Azero   0.45607
Define pAzero  0.0
Define Aplus   0.877601
Define pAplus  2.52403
Define Aminus  0.147703
Define pAminus 0.0841355
#
Alias      MyK*0       K*0
Alias      Myanti-K*0  anti-K*0
ChargeConj MyK*0       Myanti-K*0  
#
Decay B_s0sig
  1.000    MyK*0   Myanti-K*0   SVV_HELAMP Aplus pAplus Azero pAzero Aminus pAminus;
Enddecay
CDecay anti-B_s0sig
#
Decay MyK*0
  1.000 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0
#
End
