# EventType: 13196043
#
# Descriptor: [B_s0 -> (D*- -> (anti-D0 -> K+ pi-) pi-) (D+ -> K- K+ pi+)]cc
#
#
# NickName: Bs_DstD,D0_Kpi,D_KKpi=DDALITZ,DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation:  Bs decay to D*D with Dalitz decay model for D decay. 

# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Antje Moedden
# Email: antje.moedden@cern.ch
# Date: 20170419
# CPUTime: < 1 min

# -------------------------
# Define the D+ and D-
# -------------------------
Alias      MyD+  D+
Alias      MyD-  D-
ChargeConj MyD+  MyD-

# -------------------------
# Define the D*+ and D*-
# -------------------------
Alias      MyD*+       D*+
Alias      MyD*-       D*-
ChargeConj MyD*+       MyD*-

# -------------------------
# Define the D0 and D~0
# -------------------------
Alias       MyD0        D0
Alias       Myanti-D0   anti-D0
ChargeConj  MyD0        Myanti-D0

# ---------------
# Bs decay
# ---------------
Decay B_s0sig
	1.000         MyD*-    MyD+			   SVS;		
Enddecay
CDecay anti-B_s0sig

# ---------------
# D*- decay
# ---------------
Decay MyD*-
  1.000        Myanti-D0 pi- 				VSS;
Enddecay
CDecay MyD*+

# ---------------
# anti-D0 decay
# ---------------
Decay Myanti-D0
  1.000        K+        pi-                    PHSP;
Enddecay
CDecay MyD0

# ---------------
# D+ decay
# ---------------
Decay MyD+
  1.000   K- K+ pi+                  D_DALITZ;
Enddecay
CDecay MyD-
#
End

