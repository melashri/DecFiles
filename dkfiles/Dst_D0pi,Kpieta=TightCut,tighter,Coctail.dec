# EventType: 27263479
# 
# Descriptor: { [D*+ -> (D0 -> (anti-K*0 -> K- pi+) (eta -> gamma gamma) ) pi+]cc, [D*+ -> (D0 -> (anti-K*0_0 -> K- pi+) (eta -> gamma gamma) ) pi+]cc, [D*+ -> (D0-> K- (a_0+ -> pi+ (eta -> gamma gamma)) ) pi+]cc }
#
# NickName: Dst_D0pi,Kpieta=TightCut,tighter,Coctail
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#   Decay file for D* -> D0 pi+
#   where D0 decays to right-sign mode (K- pi+ eta)
#   with incoherent resonance Coctail based on Belle result arXiv:2003.07759
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Jolanta Brodzicka
# Email: Jolanta.Brodzicka@cern.ch
# Date: 20220120
# CPUTime: <1min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ D*(2010)+ => ^( D0 ==> K- pi+ ( eta -> gamma gamma ) ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV     ',
#     'inAcc       = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) )',
#     'goodD0Eta   = ( GINTREE( ("gamma"==GABSID) & (GPT > 1200 * MeV) & inAcc & inCaloAcc ) )',
#     'goodD0K     = ( ("K-"==GABSID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0Pi    = ( ("pi+"==GABSID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0      = ( (GPT > 1600 * MeV) & GINTREE(goodD0K) & GINTREE(goodD0Pi) & GINTREE(goodD0Eta) )'
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'  : 'inAcc ',
#     '[D0]cc'   : 'goodD0 '
#     }
# EndInsertPythonCode

Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
#
Alias      MyK*0 K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0 Myanti-K*0
#
#
Alias      Mya0+ a_0+
Alias      Mya0- a_0-
ChargeConj Mya0+ Mya0-
#
Alias      MyK*0(1430) K_0*0
Alias      Myanti-K*0(1430) anti-K_0*0
ChargeConj MyK*0(1430) Myanti-K*0(1430)
#
#
Alias      Myeta  eta
#
Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig

Decay MyD0
0.48   Myanti-K*0 Myeta  SVS;
0.32   Myanti-K*0(1430) Myeta  PHSP;
0.39   K- Mya0+ PHSP;
Enddecay
CDecay MyantiD0
#
Decay Myanti-K*0
1.0   K- pi+      VSS;
Enddecay
CDecay MyK*0
#
Decay Myanti-K*0(1430)
1.0   K- pi+     PHSP;
Enddecay
CDecay MyK*0(1430)
#
Decay Mya0+
1.0   pi+ Myeta      PHSP;
Enddecay
CDecay Mya0-
#
Decay Myeta
1.0     gamma gamma      PHSP;
Enddecay
#
End
 
