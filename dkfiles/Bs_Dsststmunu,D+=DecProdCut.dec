# EventType: 13574052
#
# Descriptor: {[[B_s0]nos -> (D_s1(2536)- -> (D*(2010)- -> (D--> K+ pi- mu- anti-nu_mu) pi0) K~0) mu+ nu_mu]cc, [[B_s0]os -> (D_s1(2536)+ -> (D*(2010)+ -> (D+ -> K- pi+ mu+ nu_mu) pi0) K0) mu- anti-nu_mu]cc}
#
# NickName: Bs_Dsststmunu,D+=DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation: semi-leptonic B_s0 -> D_s** mu nu decays
# EndDocumentation
#
# PhysicsWG: RD
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Hanae Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20220209
#
Alias            MyD_s2*+       D_s2*+
Alias            MyD_s2*-       D_s2*-
ChargeConj       MyD_s2*+       MyD_s2*-
#
Alias            MyD'_s1+       D'_s1+
Alias            MyD'_s1-       D'_s1-
ChargeConj       MyD'_s1+       MyD'_s1-
#
Alias            MyD0           D0
Alias            Myanti-D0      anti-D0
ChargeConj       MyD0           Myanti-D0
#
Alias            MyD*+          D*+
Alias            MyD*-          D*-
ChargeConj       MyD*+          MyD*-
#
Alias            MyD+           D+
Alias            MyD-           D-
ChargeConj       MyD+           MyD-
#
Alias            MyK*+          K*+
Alias            MyK*-          K*-
ChargeConj       MyK*+          MyK*-
#
Alias            MyK*0          K*0
Alias            Myanti-K*0     anti-K*0
ChargeConj       MyK*0          Myanti-K*0
#
Alias            Mytau+         tau+
Alias            Mytau-         tau-
ChargeConj       Mytau+         Mytau-
#
Decay B_s0sig 
  0.250          MyD'_s1-       mu+     nu_mu              ISGW2;
  0.250          MyD'_s1-       Mytau+  nu_tau             ISGW2; 
  0.250          MyD_s2*-       mu+     nu_mu              ISGW2; 
  0.250          MyD_s2*-       Mytau+  nu_tau             ISGW2; 
Enddecay
CDecay anti-B_s0sig
#
Decay MyD'_s1+
 0.850           MyD*+     K0                              VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
 0.028           MyD+      pi-       K+                    PHSP;
Enddecay
CDecay MyD'_s1-
#
Decay MyD_s2*+
  0.500          MyD+      K0                              TSS;
  0.050          MyD*+     K0                              TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyD_s2*-
#
Decay MyD*+
  0.677          MyD0      pi+                             VSS; 
  0.307          MyD+      pi0                             VSS; 
  0.016          MyD+      gamma                           VSP_PWAVE; 
Enddecay
CDecay MyD*-
#
Decay MyD-
  0.352          MyK*0               mu-   anti-nu_mu      ISGW2;
  0.019          K+        pi-       mu-   anti-nu_mu      PHSP; 
  0.010          K+ pi0    pi-       mu-   anti-nu_mu      PHSP;
Enddecay
CDecay MyD+
#
Decay MyD0
  0.341          K-                  mu+   nu_mu           ISGW2;
  0.189          MyK*-               mu+   nu_mu           ISGW2;
  0.160          K-        pi0       mu+   nu_mu           PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyK*+
  1.000          K+        pi0                             VSS;
Enddecay
CDecay MyK*-
#
Decay MyK*0
  1.000          K+        pi-                             VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Mytau+
  1.000          mu+       nu_mu     anti-nu_tau           TAULNUNU;
Enddecay
CDecay Mytau-
#
End
