# EventType: 16874041
#
# Descriptor: [Xi_b0 => (Xi_c+ -> p K- pi+) mu- anti-nu_mu]cc
#
# NickName: Xib0_Xicmunu,pKpi=cocktail,BRcorr
#
# Cuts: LHCbAcceptance
#
# Documentation: Xib0 -> Xic mu nu_mu with Xic->p K pi, includes resoannces, corrected BRs
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: < 1 min
# Responsible: S. Blusk
# Email:  sblusk@syr.edu
# Date: 20170529
#
Alias MyXi_c+ Xi_c+
Alias Myanti-Xi_c- anti-Xi_c-
ChargeConj MyXi_c+ Myanti-Xi_c-
#
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
#
Alias      MyK_0*0    K_0*0
Alias      Myanti-K_0*0 anti-K_0*0
ChargeConj MyK_0*0   Myanti-K_0*0 
#

Alias      MyDelta++  Delta++
Alias      Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++  Myanti-Delta--

Alias MyLambda(1690)0 Lambda(1690)0
Alias Myanti-Lambda(1690)0 anti-Lambda(1690)0
ChargeConj  MyLambda(1690)0 Myanti-Lambda(1690)0

Alias MyLambda(1520)0 Lambda(1520)0
Alias Myanti-Lambda(1520)0 anti-Lambda(1520)0
ChargeConj  MyLambda(1520)0 Myanti-Lambda(1520)0

Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-
#
Alias MyXi'_c+ Xi'_c+ 
Alias Myanti-Xi'_c- anti-Xi'_c-
ChargeConj MyXi'_c+ Myanti-Xi'_c- 

Alias MyXi_c*+ Xi_c*+
Alias Myanti-Xi_c*- anti-Xi_c*-
ChargeConj MyXi_c*+ Myanti-Xi_c*- 

#
#
Decay Xi_b0sig
  0.790     MyXi_c+   mu- anti-nu_mu                    PHSP;
  0.040     MyXi_c+   Mytau-  anti-nu_tau               PHSP;
  0.040    MyXi'_c+  mu- anti-nu_mu                    PHSP;
  0.002    MyXi'_c+  Mytau-  anti-nu_tau               PHSP;
  0.119     MyXi_c*+  mu- anti-nu_mu                    PHSP;
  0.006      MyXi_c*+  Mytau-  anti-nu_tau               PHSP;
Enddecay
CDecay anti-Xi_b0sig
#
Decay MyXi_c+
  0.12   MyDelta++ K-                                      PHSP;
  0.40   p+      Myanti-K*0                              PHSP;
  0.25   p+      Myanti-K_0*0                            PHSP; 
  0.13   MyLambda(1690)0 pi+                             PHSP;
  0.02   MyLambda(1520)0 pi+                             PHSP;
  0.08   p+   K-  pi+                                    PHSP;
Enddecay
CDecay Myanti-Xi_c-
#
Decay MyXi'_c+
1.0    gamma  MyXi_c+                    PHSP;
Enddecay
CDecay Myanti-Xi'_c-

Decay MyXi_c*+
0.5    gamma  MyXi_c+                    PHSP;
0.5    pi0    MyXi_c+                    PHSP;
Enddecay
CDecay Myanti-Xi_c*-

Decay MyDelta++
1.0000    p+  pi+                     PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyK_0*0
  1.000 K+ pi-                    PHSP;
Enddecay
CDecay Myanti-K_0*0
#
#
Decay MyLambda(1690)0
  1.0      p+  K-               PHSP;
Enddecay
CDecay Myanti-Lambda(1690)0

Decay MyLambda(1520)0
  1.0      p+  K-               PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0

#
Decay Mytau-
  0.1736       mu-  anti-nu_mu   nu_tau        TAULNUNU;
Enddecay
CDecay Mytau+


End

