# EventType: 49142111
#
# Descriptor: chi_c1 -> (psi(1S) -> mu+ mu-) gamma
# NickName: cep_chic1_psi1Sgamma,mumu
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/cepInAcc
# Production: SuperChic2
#
# InsertPythonCode:
# # Stop pile-up generation.
# Generation().PileUpTool = "FixedNInteractions"
#
# # SuperChic options.
# from Configurables import SuperChicProduction
# Generation().Special.addTool(SuperChicProduction)
# Generation().Special.SuperChicProduction.Commands += [
#     "xflag chic",  # Chi_c production.
#     "chiflag 1",   # Produce the 1++ chi_c state.
#     "decay 1"]     # Use the Jpsi[mu,mu] gamma decay for the chi_c.
# 
# # SuperChic2 options.
# from Configurables import SuperChic2Production
# Generation().Special.addTool(SuperChic2Production)
# Generation().Special.SuperChic2Production.Commands += [
#     "SuperChic2:proc = 22"] # Chic_c1[psi(1S)[mu,mu],gamma] production.
#
# # Cuts on the chi_c1.
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool(LoKi__FullGenEventCut, "cepInAcc")
# cepInAcc = Generation().cepInAcc
# cepInAcc.Code = "( count( out1 ) == 1 )"
# cepInAcc.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import GeV, mrad",
#     "out1 = ( ( GABSID == 20443 ) & ( GETA > 1.0 ) & ( GETA < 6.0 ) )"]
# 
# # Keep the CEP process in MCParticles.
# from Configurables import GenerationToSimulation
# GenerationToSimulation("GenToSim").KeepCode = ("( GBARCODE >= 2 )")
# EndInsertPythonCode
#
# Documentation:
# central exclusive production of chi_c1[psi(1S)[mu mu] gamma]
# EndDocumentation
#
# PhysicsWG: EW
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Philip Ilten
# Email: philten@cern.ch
# Date: 20150717
#
End
