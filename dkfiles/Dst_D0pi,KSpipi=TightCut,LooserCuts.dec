# EventType: 27165903
#
# Descriptor: [D*+ -> (D0 -> (K_S0 -> pi+ pi-) pi+ pi-) pi+]cc
#
# NickName: Dst_D0pi,KSpipi=TightCut,LooserCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D*(2010)+ => ^( D0 => ^( KS0 => ^pi+ ^pi- ) ^pi+ ^pi- ) ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter ',
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'goodD0    = ((GP > 13500 * MeV) & (GPT > 1700 * MeV) &  (GTIME > 0.018 * millimeter))',
#     'PiFromD   = ( GNINTREE( ("pi+" == GABSID ) & (GP > 1050 * MeV), 1) > 1.5 )',
#     'goodKS    = ( (GP > 2900 * MeV) & (GPT > 150 * MeV) )',
#     'PiFromKS   = ( GNINTREE( ("pi+" == GABSID ) & (GP > 1100 * MeV) , 1) > 1.5 )',
#     'goodDst   = ( (GP > 15000 * MeV) & (GPT > 1900) ) ',
#     'PiFromDst   = ( GNINTREE( ("pi+" == GABSID ) & (GP > 1100 * MeV) , 1) > 0.5 )',
#     'trigger   = ( GNINTREE( ("pi+" == GABSID) & (GPT > 900 * MeV ) & (GP > 4000 * MeV) , 4)  > 0.5) ',
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'   : 'inAcc',
#     '[D0]cc'    : 'goodD0 & PiFromD & trigger ',
#     '[D*(2010)+]cc' : 'goodDst & PiFromDst',
#     'KS0'       : 'goodKS & PiFromKS',
#     }
# EndInsertPythonCode
#
# Documentation: Inclusive production of D*+. D* is forced to decay to D0 pi+, then D0 to (KS pi+ pi-) as phase space,
# then KS to (pi+ pi-) as phase space. Decay products in acceptance.
# 
# EndDocumentation
#
# CPUTime: <1min
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Nathan Jurik
# Email: nathan.jurik@cern.ch
# Date: 20191209
#
Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
Alias myK_S0  K_S0
ChargeConj myK_S0 myK_S0
#
Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig
#
Decay MyD0
  1.000  myK_S0 pi+  pi-        PHSP;
Enddecay
CDecay MyantiD0
#
Decay myK_S0
1.000     pi+  pi-                      PHSP;
Enddecay
#
End
#
