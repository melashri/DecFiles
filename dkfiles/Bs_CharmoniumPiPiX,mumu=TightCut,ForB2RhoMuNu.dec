# EventType: 13444023
# Descriptor: [Bs -> (Charmonium -> mu+ mu- X) pi+ pi- X]cc 
#
# NickName: Bs_CharmoniumPiPiX,mumu=TightCut,ForB2RhoMuNu
# 
# Cuts: LoKi::GenCutTool/TightCut 
# 
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# SignalFilter = gen.TightCut
# SignalFilter.Decay = "^( (Beauty & LongLived) --> ( ( J/psi(1S) | psi(2S) ) --> mu+ mu- ...) pi+ pi- ...)"
# SignalFilter.Preambulo += [
#   "from GaudiKernel.SystemOfUnits import  GeV",
#   "inAcc                = in_range ( 0.005 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )", 
#   "muCuts               = (0 < GNINTREE ( ('mu-' == GABSID ) & (GP > 5 * GeV) &  (GPT > 1.2 * GeV)  & inAcc))",
#   "piPlusCuts           = (0 < GNINTREE ( ('pi+' == GID ) & (GP > 1.5 * GeV) & (GPT > 0.35 * GeV)  & inAcc ))",
#   "piMinusCuts          = (0 < GNINTREE ( ('pi-' == GID ) & (GP > 1.5 * GeV) & (GPT > 0.35 * GeV)  & inAcc ))",
#   "piMaxPT              = (GMAXTREE( GPT, ('pi+' == GABSID) & inAcc & (GP > 1.5 * GeV)) > 0.85 * GeV )",
#   "piMaxP               = (GMAXTREE( GP, ('pi+' == GABSID) & inAcc & (GPT > 0.35 * GeV)) > 4.5 * GeV )",
#   "allcuts              = ( muCuts & piPlusCuts & piMinusCuts & piMaxPT & piMaxP )"
#   ]
# SignalFilter.Cuts =  { "Beauty" : "allcuts" }
# EndInsertPythonCode
#
# Documentation: Bs -> J/psi pi+ pi- X events, with cuts optimised for B -> rho mu nu analysis.
# EndDocumentation 
# 
# PhysicsWG: B2SL 
# Tested: Yes 
# CPUTime: 2min
# Responsible: Michel De Cian
# Email: michel.de.cian@cern.ch 
# Date: 20220109
#
#
Define Hp 0.49 
Define Hz 0.775 
Define Hm 0.4 
Define pHp 2.50 
Define pHz 0.0 
Define pHm -0.17
#
Alias MyJ/psi J/psi 
ChargeConj MyJ/psi MyJ/psi 
# 
Alias Mychi_c1 chi_c1 
ChargeConj Mychi_c1 Mychi_c1 
#
Alias Mychi_c0 chi_c0
ChargeConj Mychi_c0 Mychi_c0
#
Alias Mychi_c2 chi_c2
ChargeConj Mychi_c2 Mychi_c2
#
Alias      Myphi   phi 
ChargeConj Myphi   Myphi
#
Alias      MyEta  eta
ChargeConj MyEta  MyEta
#
Alias      MyEtaPrime  eta'
ChargeConj MyEtaPrime  MyEtaPrime
#
Alias      MyOmega  omega
ChargeConj MyOmega  MyOmega
#
Alias      Myf_0  f_0
ChargeConj Myf_0  Myf_0
#
Alias      Myf_1  f_1
ChargeConj Myf_1  Myf_1
#
Alias      Myf'_2 f'_2
ChargeConj Myf'_2 Myf'_2
# 
Alias Mypsi(2S) psi(2S) 
ChargeConj Mypsi(2S) Mypsi(2S) 
#
Decay Mychi_c1
  0.3430	MyJ/psi	gamma				VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
#
Decay Mychi_c2
  0.1900	MyJ/psi	gamma				PHSP ;
Enddecay
#
Decay Mychi_c0
  0.0140	MyJ/psi	gamma				SVP_HELAMP 1.0 0.0 1.0 0.0 ;
Enddecay
#
Decay Myphi   
  0.1524      pi+	pi-	pi0			PHSP; 
Enddecay
#
Decay MyEta   
  0.2292      pi+	pi-	pi0			PHOTOS ETA_DALITZ;
  0.0422      pi+ 	pi- 	gamma			PHOTOS PHSP; 
Enddecay
#
Decay MyEtaPrime
  0.4320   pi+       pi-     eta            PHOTOS PHSP; # don't force the eta, as we have 2 charged pions already
  0.2170   pi0       pi0     MyEta          PHOTOS PHSP; # force the eta to have 2 charged pions
  0.293511 rho0      gamma                  PHOTOS SVP_HELAMP  1.0 0.0 1.0 0.0;
  0.0275   MyOmega   gamma                  PHOTOS SVP_HELAMP  1.0 0.0 1.0 0.0; # force the omega to have 2 charged pions
  0.0036   pi+       pi-     pi0            PHOTOS PHSP;  
  0.0024   pi+       pi-     e+      e-     PHOTOS PHSP;  
Enddecay
#
Decay MyOmega
  0.893        pi-      pi+      pi0        PHOTOS OMEGA_DALITZ;
  0.0153       pi-      pi+                 PHOTOS VSS;
Enddecay
#
Decay Myf'_2   
  0.116	       MyEta	eta			PHSP;  # force one eta to have at least 2 charged pions
Enddecay
#
Decay Myf_0
  1.000         pi+	pi-			PHSP ;
Enddecay
#
Decay Myf_1
  0.218	pi+	pi-	pi0	pi0		PHSP ;
  0.109	rho0  	pi+	pi-			PHSP ;
  0.0867	eta   	pi+    	pi-    		PHSP;
  0.0433 	MyEta   pi0     pi0    		PHSP;
  0.055	rho0    gamma                  		PHOTOS VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ; 
Enddecay
#
Decay MyJ/psi
  1.0000  mu+        mu-				PHOTOS VLL ;
Enddecay
#
Decay Mypsi(2S)
  0.0080	mu+	mu-				PHOTOS VLL;
  0.3467	MyJ/psi	pi+	pi-			PHOTOS VVPIPI ; 
  0.1823  	MyJ/psi	pi0    	pi0                    	VVPIPI ;   
  0.0337  	MyJ/psi 	eta                    	PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ; 
  0.0979  	Mychi_c0	gamma                  	PHSP ; 
  0.0975  	Mychi_c1	gamma                  	VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ; 
  0.0952  	Mychi_c2	gamma                  	PHSP ; 
Enddecay
#
Decay B_s0sig
  0.00108	MyJ/psi		Myphi				PVV_CPLH 0.02 1 Hp pHp Hz pHz Hm pHm;
  0.00040 	MyJ/psi   	MyEta 	 	  		SVS ;
  0.00033 	MyJ/psi   	MyEtaPrime 			SVS ; 
  0.00026	MyJ/psi   	Myf'_2			      	PHSP ;			
  0.00013 	MyJ/psi   	Myf_0				SVS ; 				
  0.00002 	MyJ/psi   	pi+	pi-			PHSP ; 				
  0.00008 	MyJ/psi   	pi+ 	pi- 	pi+ 	pi-	PHSP ; 				
  0.00007 	MyJ/psi   	Myf_1			      	SVV_HELAMP Hp pHp Hz pHz Hm pHm; 
  0.00054	Mypsi(2S)	Myphi				PVV_CPLH 0.02 1 Hp pHp Hz pHz Hm pHm;				
  0.00033 	Mypsi(2S) 	MyEta 				SVS;
  0.00013 	Mypsi(2S) 	MyEtaPrime 			SVS;
  0.00007 	Mypsi(2S) 	pi+ pi- 			PHSP;
  0.00011 	Mychi_c1  	Myphi 				SVV_HELAMP Hp pHp Hz pHz Hm pHm;
Enddecay 
CDecay anti-B_s0sig 
# 
End
