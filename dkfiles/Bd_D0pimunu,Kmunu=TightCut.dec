# EventType: 11574002 
#
# Descriptor: {[[B0]nos -> (anti-D0 -> K+ mu- anti-nu_mu) pi- mu+ nu_mu]cc, [[B0]os -> (anti-D0 -> K+ mu- anti-nu_mu) pi- mu+ nu_mu]cc}
#
# NickName: Bd_D0pimunu,Kmunu=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Gauss.Configuration import *
# from Configurables import LoKi__GenCutTool
#
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "([(B0) ==> ^K+ ^mu- nu_mu~ ^pi- ^mu+ nu_mu {X} {X} {X} {X} {X} {X} {X}]CC)"
# tightCut.Preambulo += ["from LoKiCore.functions import in_range", "from GaudiKernel.SystemOfUnits import MeV"]
# tightCut.Cuts      =    {
#     '[mu-]cc'     : "(in_range(0.01, GTHETA, 0.4))",
#     '[K+]cc'      : "(in_range(0.01, GTHETA, 0.4))",
#     '[pi-]cc'	    : "(in_range(0.01, GTHETA, 0.4))",
#     '[mu+]cc'	    : "(in_range(0.01, GTHETA, 0.4))"
#   }
# EndInsertPythonCode
#
# Documentation: semi-leptonic B0 -> D0 pi mu nu decays
# EndDocumentation
#
# PhysicsWG: RD
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Mark Smith
# Email: mark.smith@cern.ch
# Date: 20220411
#
Alias            MyD*+          D*+
Alias            MyD*-          D*-
ChargeConj       MyD*+          MyD*-
#
Alias            MyD0           D0
Alias            Myanti-D0      anti-D0
ChargeConj       MyD0           Myanti-D0
#
Decay B0sig 
  0.0043          Myanti-D0       pi-      mu+         nu_mu         PHSP;
  0.0493          MyD*-      mu+         nu_mu         PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyD*+
  0.677          MyD0           pi+                               VSS; 
Enddecay
CDecay MyD*-
#

Decay MyD0 
  0.341          K-             mu+     nu_mu                     ISGW2;
Enddecay
CDecay Myanti-D0
#
End
