# EventType: 15574141
# 
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> (Lambda0 -> p+ pi-) e+ nu_e)  anti-nu_mu mu-]cc
# 
# NickName: Lb_Lcmunu,L0enu=DecProdCut,HighVisMass,EvtGenCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^e+ ^nu_e) ^mu- ^nu_mu~)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[Lambda_b0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += ["visMass  = ( ( GMASS ( 'mu+' == GABSID , 'e-' == GABSID, 'p+' == GABSID, 'pi+' == GABSID ) ) > 4500 * MeV ) " ]
#
# EndInsertPythonCode
#
# Documentation:Semi-leptonic Lambda B decay into Lc mu Nu. Lc is forced to Lambda0 e+ nu, and Lambda0 forced to pi+ pi-.
# Generator level cut applied to have a visible mass larger than 4.5 GeV, for Lb->Lemu and Lb->Lee.
# EndDocumentation
#
# CPUTime: 4 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Mick Mulder   
# Email: mick.mulder@cern.ch
# Date: 20180816
#
#
Alias      MyLambda0      Lambda0
Alias      Myanti-Lambda0 anti-Lambda0
ChargeConj MyLambda0      Myanti-Lambda0
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
###
Decay Lambda_b0sig
  1.000    MyLambda_c+        mu-  anti-nu_mu     PHOTOS   BaryonPCR  1 1 1 1;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
  1.000   MyLambda0 e+ nu_e	       PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyLambda0
  1.000   p+          pi-    PHSP;
Enddecay
#
Decay Myanti-Lambda0
  1.000   anti-p-    pi+     PHSP;
Enddecay 
#
End
