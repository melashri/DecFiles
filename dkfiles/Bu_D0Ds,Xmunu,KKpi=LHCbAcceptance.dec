# EventType:  12995610
#
# Descriptor: [[B+] -> (D*(2007)~0 -> (D~0 -> K+ mu- nu_mu~) pi0) (D*_s+ -> (D_s+ -> K+ K- pi+) gamma)]cc
#
# NickName: Bu_D0Ds,Xmunu,KKpi=LHCbAcceptance
#
# Cuts: LHCbAcceptance
#
# Documentation: Sum of Bu decaying to D0 Ds, Ds forced to K pi pi, D0 forced to SL. 
#
# EndDocumentation
#
# CPUTime: <1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Scott Ely
# Email: seely@syr.edu
# Date: 20180327
#
Alias        MyD0            D0
Alias        MyAntiD0        anti-D0
ChargeConj   MyD0            MyAntiD0
#
Alias        MyD*0            D*0
Alias        MyAntiD*0        anti-D*0
ChargeConj   MyD*0           MyAntiD*0
#
Alias        MyD_s+          D_s+
Alias        MyD_s-          D_s-
ChargeConj   MyD_s+          MyD_s-
#
Alias        MyD_s*+         D_s*+
Alias        MyD_s*-         D_s*-
ChargeConj   MyD_s*+         MyD_s*-
#
Alias        MyD_10          D_10
Alias        MyAntiD_10      anti-D_10
ChargeConj   MyD_10          MyAntiD_10
#
Alias        MyD'_10         D'_10
Alias        MyAntiD'_10     anti-D'_10
ChargeConj   MyD'_10         MyAntiD'_10
#
Alias        MyD_2*0         D_2*0
Alias        MyAntiD_2*0     anti-D_2*0
ChargeConj   MyD_2*0         MyAntiD_2*0
#
Alias        MyD*+           D*+
Alias        MyD*-           D*-
ChargeConj   MyD*+           MyD*-
#
Decay B+sig
###BFs come from excel sheet, very similar to DECAY.DEC
 0.00900                MyAntiD0        MyD_s+          PHSP;
 0.00760                MyD_s*+         MyAntiD0        SVS;
 0.00820                MyAntiD*0       MyD_s+          SVS;
 0.01710                MyAntiD*0       MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00120                MyAntiD'_10     MyD_s+          SVS;
 0.00240                MyAntiD'_10     MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00240                MyAntiD_10      MyD_s+          SVS;
 0.00480                MyAntiD_10      MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00840                MyAntiD_2*0     MyD_s+          STS;
 0.00800                MyAntiD_2*0     MyD_s*+         PHSP;

 0.00180                MyAntiD0        MyD_s+    pi0           PHSP;
 0.00180                MyAntiD0        MyD_s*+   pi0           PHSP;
 0.00330                MyAntiD0        MyD_s+    pi+ pi-       PHSP;
 0.00080                MyAntiD0        MyD_s+    pi0 pi0       PHSP;
 0.00330                MyAntiD0        MyD_s*+   pi+ pi-       PHSP;
 0.00080                MyAntiD0        MyD_s*+   pi0 pi0       PHSP;
Enddecay
CDecay B-sig
#
Decay MyD_s+
 1.000		K+	K-	pi+	PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
 0.9350         MyD_s+  gamma           VSP_PWAVE;
 0.0580         MyD_s+  pi0             VSS;
Enddecay
CDecay MyD_s*-
#
Decay MyD0
 0.03333		K-	mu+	nu_mu			PHOTOS ISGW2;
 0.0192		K*-	mu+	nu_mu			PHOTOS ISGW2;
 0.000076	K_1-	mu+	nu_mu			PHOTOS ISGW2;
 0.00110	K_2*-	mu+	nu_mu			PHOTOS ISGW2;
 0.00238	pi-	mu+	nu_mu			PHOTOS PHSP;
 0.00040	K-	pi0	mu+	nu_mu		PHOTOS PHSP;
 0.00120	K-	pi+	pi-	mu+	nu_mu  	PHOTOS PHSP;
Enddecay
CDecay MyAntiD0
#
Decay MyD*0
 0.647          MyD0    pi0             PHOTOS VSS;
 0.353          MyD0    gamma           PHOTOS VSP_PWAVE;
Enddecay
CDecay MyAntiD*0
#
Decay MyD_10
 0.667          MyD*+   pi-             VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
 0.333          MyD*0   pi0             VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyAntiD_10
#
Decay MyD*+
 1.000          MyD0    pi+             VSS;
Enddecay
CDecay MyD*-
#
Decay MyD'_10
 0.667          MyD*+   pi-             VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
 0.333          MyD*0   pi0             VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyAntiD'_10
#
Decay MyD_2*0
###Only 1st one is relevant in PDG. 2nd in DECAY.DEC
 0.2090         MyD*+   pi-             TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
 0.1030         MyD*0   pi0             TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
###The two below are listed as not seen in pdg and are thus commented.
### 0.          MyD0    pi+   pi-       PHOTOS PHSP;
### 0.          MyD*0   pi+   pi-       PHOTOS PHSP;
Enddecay
CDecay MyAntiD_2*0
#
End
