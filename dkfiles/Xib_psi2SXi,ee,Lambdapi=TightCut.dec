# EventType: 16155131
#
# Descriptor: [Xi_b- -> (Xi- -> (Lambda0 -> p+ pi-) pi-) (psi(2S) -> e+ e-)]cc
#
# NickName: Xib_psi2SXi,ee,Lambdapi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: psi(2S) forced to go to e+ e-,  Xi forced to go Lambda pi, Lambda forced to go to p pi.
# Tight generator cuts on the Xi and its decay products.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[(Xi_b- => ^(Xi- => ^(Lambda0 => ^p+ ^pi-) ^pi-) (psi(2S) => ^e+ ^e-))]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, GeV" ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )     " ,
#     "inAccH       =  in_range ( 0.001 , GTHETA , 0.390 )     " ,
#     "inEtaL       =  in_range ( 1.5  , GETA   , 5.5 )        " , 
#     "inEtaLD      =  in_range ( 1.5  , GETA   , 7.5 )        " ,
#     "inP_pi       =  ( GP > 1.5  *  GeV ) ",
#     "inP_p        =  ( GP > 1.5 *  GeV ) ",
#     "inP_el       =  ( GP > 2.  *  GeV ) ",
#     "goodElectron =  inAcc  & inP_el & inEtaL " ,
#     "goodPion     =  inAccH & inP_pi & inEtaLD" ,
#     "goodProton   =  inAccH & inP_p  & inEtaLD" ,
#     "GVZ = LoKi.GenVertices.PositionZ()       " ,
#     "decay = in_range ( -1.1 * meter, GFAEVX ( GVZ, 100 * meter ), 3 * meter )",
# ]
# tightCut.Cuts      =    {
#     "[Xi-]cc"      : "decay", 
#     "[Lambda0]cc"  : "decay",
#     "[pi-]cc"      : "goodPion" , 
#     "[p+]cc"       : "goodProton",
#     "[e+]cc"       : "goodElectron"
#                         }
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Niladri Sahoo
# Email: Niladri.Sahoo@cern.ch
# Date: 20211025
# CPUTime: 2 min
#
Alias      MyXi     Xi-
Alias      Myanti-Xi anti-Xi+
ChargeConj Myanti-Xi MyXi
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
#
Alias      Mypsi2S       psi(2S)
ChargeConj Mypsi2S       Mypsi2S
# 
Decay Xi_b-sig 
  1.000    MyXi          Mypsi2S      PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay Mypsi2S
  1.000     e+  e-                    PHOTOS VLL;
Enddecay
#
Decay MyXi
  1.000     MyLambda   pi-            HELAMP   0.551  0.0  0.834  0.0;
Enddecay
CDecay Myanti-Xi
#
Decay MyLambda
  1.000     p+   pi-                  HELAMP   0.936   0.0   0.351   0.0;
Enddecay
CDecay Myanti-Lambda
#
End
