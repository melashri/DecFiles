# EventType: 13112204
#
# Descriptor: [B_s0 -> gamma mu+ mu-]cc
#
# NickName: Bs_gammamumu=ISRandFSR,DecProdCut,TightCut
# 
# Documentation:
#     m(mumu) > 1080
#     ISR and FSR contributions 
#     Input parameters mu         - the scale parameter (in GeV's)
#                      Nf         - number of "effective" flavors (for b-quark Nf=5) 
#                      sr         - state radiation type
#                      res_swch   - resonant switching parametr
#                      ias        - switching parametr for \alpha_s(M_Z) value
#                      Wolfenstein parameterization for CKM matrix
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Meril Reboud
# Email: meril.reboud@{nospam}cern.ch
# Date: 20170322
# CPUTime: 5 min
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = "^[ B_s0 ==> ^mu+ ^mu- ^gamma ]CC"
# tightCut.Cuts      =    {
#     '[mu-]cc'             : ' inAcc & ( GPT > 200 * MeV )  ' ,
#     '[mu+]cc'             : ' inAcc & ( GPT > 200 * MeV )  ' ,
#     'gamma'               : ' goodGamma '	,
#     '[B_s0]cc'            : ' massCut ' }
# tightCut.Preambulo += [
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEcalX    = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY    = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'goodGamma  = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY ' ,
#     "massCut    = GMASS('mu+' == GID , 'mu-' == GID) > 1080 * MeV" ]
#
# EndInsertPythonCode
#
#
Define mu 5.0
Define Nf 5
Define sr 2
Define res_swch 1
Define ias 1
Define Egamma 0.02
Define A 0.811
Define lambda 0.22506
Define barrho 0.124
Define bareta 0.356
#


Decay B_s0sig
   1.000    gamma   mu+   mu-   BSTOGLLISRFSR mu Nf sr res_swch ias Egamma A lambda barrho bareta;
Enddecay
CDecay anti-B_s0sig

End
#
