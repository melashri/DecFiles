# EventType: 27162212
#
# Descriptor: [D*0 -> (D0 -> K- pi+) gamma]cc
#
# NickName: Dst0_D0gamma,Kpi=TightCut,gammaConv
# 
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen=Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# 
# tightCut = Generation().SignalPlain.TightCut
# tightCut.Decay     = '^[D*(2007)0 => ^(D0 ==> ^K- ^pi+) ^gamma]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV" ,
#     "from LoKiCore.functions import in_range"
# ]
#tightCut.Cuts = {
#   '[D*(2007)0]cc'   : '(GP >  6000 * MeV)',
#   '[D0]cc'          : '(GP >  3000 * MeV)',
#   '[K-]cc'          : '(GP >  1500 * MeV) & (GPT > 50 * MeV) & (in_range(0.010, GTHETA, 0.400))',
#   '[pi+]cc'         : '(GP >  1500 * MeV) & (GPT > 50 * MeV) & (in_range(0.010, GTHETA, 0.400))',
#   'gamma'           : '(GP >  2500 * MeV) & (in_range(0.010, GTHETA, 0.400))'
#   }
# EndInsertPythonCode
#
# PostFilter: ConversionFilter
# PostFilterOptions: Mother "D*(2007)0" MinP 1500 MinPT 50 MaxSearchDepth 1
#
# Documentation: D*0 -> D0gamma decay file, asking the final state particles to be in the acceptance and have enough momentum to make it into long tracks. Filter such that the gamma converts, and the electrons have enough momentum to make it into long tracks.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Michel De Cian
# Email: michel.de.cian@cern.ch
# Date: 20190522
# CPUTime: < 1min
#
Alias      Myanti-D0   anti-D0
Alias      MyD0        D0
ChargeConj MyD0        Myanti-D0
##
Decay D*0sig
1.00 MyD0  gamma    VSP_PWAVE;
Enddecay
CDecay anti-D*0sig
#
Decay MyD0
1.00     K-  pi+        PHSP;
Enddecay
CDecay Myanti-D0
#
End

