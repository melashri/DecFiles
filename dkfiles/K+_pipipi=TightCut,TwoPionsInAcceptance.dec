# EventType: 37103002
#
# Descriptor: [ K+ => pi+ pi- pi+ ]cc
#
# NickName: K+_pipipi=TightCut,TwoPionsInAcceptance
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Forces a K+ to pi+ pi- pi+, with at least two pions in the acceptance, with minimum p / pT
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[^(K+ => pi+ pi- pi+)]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV                          " ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )                                   " ,
#     "inEta        =  in_range ( 1.95  , GETA   , 5.050 )                                   " ,
#     "fastTrack    =  (GP > 1.3 * GeV ) & ( GPT > 100 * MeV )                               " ,
#     "nGoodDaus    =  GNINTREE( ('pi+' == GABSID) & inEta & fastTrack & inAcc )             " ,
#     "okDau        =  (nGoodDaus > 1)                                                       " , 
#     "GVX          = LoKi.GenVertices.PositionX()                                           " ,
#     "GVY          = LoKi.GenVertices.PositionY()                                           " ,
#     "GVZ          = LoKi.GenVertices.PositionZ()                                           " ,
#     "vx           = GFAEVX ( GVX, 100 * meter )                                            " ,    
#     "vy           = GFAEVX ( GVY, 100 * meter )                                            " ,
#     "rho2         = vx**2 + vy**2                                                          " ,
#     "rhoK         =  rho2 < ( 1000 * millimeter )**2                                       " , 
#     "decay        = in_range ( -0.1 * meter,  GFAEVX ( GVZ, 100 * meter ),  2.27 * meter ) " ,
#     "positivez    = ( GPZ > 0 * MeV )                                                      " ,
#     "kpt          = ( GPT > 90 *MeV )                                                      " ,
# ]
# tightCut.Cuts      =    {
#     '[K+]cc'  : ' decay & rhoK & positivez & kpt & okDau '
#                         }
# EndInsertPythonCode
#
# PhysicsWG: Sim
# Tested: Yes
# Responsible:   Michel De Cian
# Email: michel.de.cian@cern.ch
# Date:   20190820
# CPUTime: < 1 min
#
Decay       K+sig
  1.000      pi+ pi- pi+     PHSP;
Enddecay
CDecay K-sig
#
End
