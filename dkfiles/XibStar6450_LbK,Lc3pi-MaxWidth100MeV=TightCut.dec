# EventType: 16467003
# NickName: XibStar6450_LbK,Lc3pi-MaxWidth100MeV=TightCut
# Descriptor: [Sigma_b- -> K- (Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi- pi+ pi-)]cc
#
# Documentation:
#   Decay Xib**- -> Lb0 K- with Lb0 -> Lc+ pi- pi+ pi- and Lc+ --> p K- pi+
#   Mass = 6.45 GeV, Width = 35 MeV, maxWidth 100 MeV   
# EndDocumentation
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay     = '[Sigma_b- ==> (Lambda_b0 ==> (Lambda_c+  --> ^p+ ^K- ^pi+) ^pi- ^pi+ ^pi-) K- ]CC'
# tightCut.Cuts      =    {
#     '[K-]cc'   : ' goodKaon ' ,
#     '[p+]cc'   : ' goodProton ' ,
#     '[pi+]cc'  : ' goodpi  ' ,
#     '[pi-]cc'  : ' goodpi  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import GeV',
#     'inAcc    = in_range ( 0.010 , GTHETA , 0.400 ) ' ,
#     'goodKaon = ( GPT > 0.09 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodProton = ( GPT > 0.09 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodpi  = ( GPT > 0.09 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ]
#
# EndInsertPythonCode
#
#
# ParticleValue: " Sigma_b-   114   5112 -1.0  6.450  1.88000e-023       Sigma_b-   5112  100.00", " Sigma_b~+  115  -5112  1.0  6.450  1.880000e-023  anti-Sigma_b+  -5112  100.00"
#
# Email: sblusk@syr.edu
# PhysicsWG: Onia
# Tested: Yes
# CPUTime: <1min
# Responsible: Steve Blusk
# Date: 20210621
#

Alias MyLambda_b0       Lambda_b0
Alias Myanti-Lambda_b0  anti-Lambda_b0
ChargeConj MyLambda_b0  Myanti-Lambda_b0

Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
Alias      MyK*0          K*0
Alias      Myanti-K*0     anti-K*0
ChargeConj MyK*0          Myanti-K*0
#
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0
Alias MyLambda_c(2593)+ Lambda_c(2593)+
Alias Myanti-Lambda_c(2593)- anti-Lambda_c(2593)-
ChargeConj MyLambda_c(2593)+ Myanti-Lambda_c(2593)-
#
Alias MyLambda_c(2625)+ Lambda_c(2625)+
Alias Myanti-Lambda_c(2625)- anti-Lambda_c(2625)-
ChargeConj MyLambda_c(2625)+ Myanti-Lambda_c(2625)-
#
Alias MySigma_c0 Sigma_c0
Alias Myanti-Sigma_c0 anti-Sigma_c0
ChargeConj MySigma_c0 Myanti-Sigma_c0
#
Alias MySigma_c++ Sigma_c++
Alias Myanti-Sigma_c-- anti-Sigma_c--
ChargeConj MySigma_c++ Myanti-Sigma_c--
#
Alias      Myf_2 f_2
ChargeConj Myf_2 Myf_2
#
Alias      Myrho0   rho0
ChargeConj Myrho0   Myrho0
#
Alias      Mya_1-     a_1-
Alias      Mya_1+     a_1+
ChargeConj Mya_1+     Mya_1-
#

# Force Sb- (stand-in for Xib**-) to decay to Lb0 K-:
Decay Sigma_b-sig
1.000    MyLambda_b0         K-     PHSP;
Enddecay
CDecay anti-Sigma_b+sig

Decay MyLambda_b0
  0.53    MyLambda_c+        Mya_1-         PHSP;
  0.10    MyLambda_c+        Myrho0  pi-    PHSP;
  0.14    MyLambda_c+        Myf_2   pi-    PHSP;
  0.03    MyLambda_c(2593)+  pi-            PHSP;
  0.06    MyLambda_c(2625)+  pi-            PHSP;
  0.05    MySigma_c++        pi-  pi-       PHSP;
  0.09    MySigma_c0         pi+  pi-       PHSP;
Enddecay
CDecay Myanti-Lambda_b0

Decay MyLambda_c+
  0.02800         p+      K-      pi+          PHSP;
  0.01065         p+      Myanti-K*0           PHSP;
  0.00860         Delta++ K-                   PHSP;
  0.00414         MyLambda(1520)0 pi+          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
# BR = 1
#
Decay MyLambda(1520)0
  0.23   p+     K-                             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#

Decay Mya_1+
  1.000   Myrho0 pi+       VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay MyLambda_c(2593)+
  0.24000      MySigma_c++         pi-                      PHSP; 
  0.24000      MySigma_c0          pi+                      PHSP;
  0.18000      MyLambda_c+         pi+    pi-               PHSP;
Enddecay
CDecay Myanti-Lambda_c(2593)-
#
Decay MyLambda_c(2625)+
  1.0000     MyLambda_c+  pi+  pi-            PHSP;
Enddecay
CDecay Myanti-Lambda_c(2625)-
#
Decay MySigma_c++
  1.0000    MyLambda_c+  pi+                  PHSP;
Enddecay
CDecay Myanti-Sigma_c--
#
Decay MySigma_c0
  1.0000    MyLambda_c+  pi-                  PHSP;
Enddecay
CDecay Myanti-Sigma_c0
#
Decay Myf_2
  1.0000  pi+ pi-                             TSS;
Enddecay
#
Decay MyK*0
  1.000   K+  pi-                             VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Myrho0
  1.0000  pi+ pi-                             VSS;
Enddecay
#


End



