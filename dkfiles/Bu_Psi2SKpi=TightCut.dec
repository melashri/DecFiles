# EventType: 12243410
#
# Descriptor: [B+ -> (psi(2S) -> mu+ mu-) (K*(892)+ -> K+ pi0)]cc
#
# ParticleValue: "K*_0(1430)+ 147 10321 1 0.845 -0.468 K_0*+ 10321 0", "K*_0(1430)- 148 -10321 -1 0.845 -0.468 K_0*- -10321 0"
#
# NickName: Bu_Psi2SKpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: B+ -> psi(2S) K+ Pi0 including several intermediate K*+,
#     where psi(2S) -> mu+ mu-. Relative proportion of K* resonance
#     components is taken from Belle paper arXiv:1306.4894v3
#     Included intermediate state:
#     K_0(800)+ aka K_0(700)+ aka Kappa is defined as K*_0(1430)+ with
#     mass of 845 MeV/c^2 and width 468 MeV decaying with PHSP decay model
#     other K*(892)+, K*(1410)+, K2*(1430)+ and K*(1680)+ resonances are
#     defined directly.
#
#     Tight generator level cuts applied for all particles except pi0,
#     which increases the statistics with the factor of ~2.
# EndDocumentation
#
# Sample: SignalRepeatedHadronization
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalRepeatedHadronization 
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut   = signal.TightCut
# tightCut.Decay = '^[B+ ==> ^(psi(2S) => ^mu+ ^mu-) ^K+ pi0]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV',
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 )                       ',
#     'inEta     = in_range ( 1.95  , GETA   , 5.050 )                       ',
#     'inY       = in_range ( 1.9   , GY     , 4.6   )                       ',
#     'fastTrack = ( GPT > 180 * MeV ) & in_range( 2.9 * GeV, GP, 210 * GeV )',
#     'goodTrack = inAcc & inEta & fastTrack                                 ',
#     'goodPsi   = inY                                                       ',
#     'longLived = 75 * micrometer < GTIME                                   ',
#     'goodB     = inY & longLived                                           ',
# ]
# tightCut.Cuts = {
#     '[B+]cc' : 'goodB                          ',
#     'psi(2S)': 'goodPsi                        ',
#     '[K+]cc' : 'goodTrack                      ',
#     '[mu+]cc': 'goodTrack & ( GPT > 500 * MeV )'
# }
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Slava Matiunin
# Email: Viacheslav.Matiunin@<no-spam>cern.ch
# Date: 20211029
# CPUTime: <1 min
#
Define PKHplus 0.159
Define PKHzero 0.775
Define PKHminus 0.612
Define PKphHplus 1.563
Define PKphHzero 0.000
Define PKphHminus 2.712
#
Alias      Mypsi(2S) psi(2S)
ChargeConj Mypsi(2S) Mypsi(2S)
#
## K*(800)+
Alias      My1K*+ K_0*+
Alias      My1K*- K_0*-
ChargeConj My1K*+ My1K*-
#
## K*(892)+
Alias      My2K*+ K*+
Alias      My2K*- K*-
ChargeConj My2K*+ My2K*-
#
## K*(1410)+
Alias      My3K*+ K'*+
Alias      My3K*- K'*-
ChargeConj My3K*+ My3K*-
#
## K2*(1430)+
Alias      My4K*+ K_2*+
Alias      My4K*- K_2*-
ChargeConj My4K*+ My4K*-
#
## K*(1680)+
Alias      My5K*+ K''*+
Alias      My5K*- K''*-
ChargeConj My5K*+ My5K*-
#
Decay B+sig
    ## non-resonant
    0.2500  Mypsi(2S) K+     pi0  PHSP ;
    ## resonances
    0.0525  Mypsi(2S) My1K*+      SVS ;
    0.5779  Mypsi(2S) My2K*+      SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.0389  Mypsi(2S) My3K*+      SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.0408  Mypsi(2S) My4K*+      PHSP ;
    0.0399  Mypsi(2S) My5K*+      SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
Enddecay
CDecay B-sig
#
Decay Mypsi(2S)
    1.0000  mu+       mu-         PHOTOS VLL ;
Enddecay
#
## K*(800)+
Decay My1K*+
    1.0000  K+        pi0         PHSP ;
Enddecay
Decay My1K*-
    1.0000  K-        pi0         PHSP ;
Enddecay
#
## K*(892)+
Decay My2K*+
    1.0000  K+        pi0         VSS ;
Enddecay
Decay My2K*-
    1.0000  K-        pi0         VSS ;
Enddecay
#
## K*(1410)+
Decay My3K*+
    1.0000  K+        pi0         VSS ;
Enddecay
Decay My3K*-
    1.0000  K-        pi0         VSS ;
Enddecay
#
## K2*(1430)+
Decay My4K*+
    1.0000  K+        pi0         TSS ;
Enddecay
Decay My4K*-
    1.0000  K-        pi0         TSS ;
Enddecay
#
## K*(1680)+
Decay My5K*+
    1.0000  K+        pi0         VSS ;
Enddecay
Decay My5K*-
    1.0000  K-        pi0         VSS ;
Enddecay
#
End
#
