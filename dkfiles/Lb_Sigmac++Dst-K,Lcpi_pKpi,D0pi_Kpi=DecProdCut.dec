# EventType: 15298006
# 
# Descriptor: [Lambda_b0 -> K- (D*- -> (anti-D0 -> K+ pi-) pi-) (Sigma_c*++ -> (Lambda_c+ -> p+ K- pi+) pi+)]cc  
# 
# NickName: Lb_Sigmac++Dst-K,Lcpi_pKpi,D0pi_Kpi=DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation: Lb -> Sigma_c++ D*- K- with Sigma_c++ -> Lc+ pi+, Lc -> p K pi and D*- -> D0bar K-, D0bar->K pi, decay products in acceptance.
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# CPUTime: <1min
# Responsible: Liming Zhang
# Email: liming.zhang@cern.ch
# Date: 20190103
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
Alias MyD*-   D*-
Alias MyD*+   D*+
ChargeConj MyD*-  MyD*+
#
Alias      MyD0          D0                                                                                                                                                                                  
Alias      Myanti-D0     anti-D0                                                                                                                                                                             
ChargeConj Myanti-D0     MyD0
#
Alias MySigma_c++       Sigma_c++
Alias Myanti-Sigma_c--  anti-Sigma_c--
ChargeConj MySigma_c++  Myanti-Sigma_c--
#
Alias MySigma_c*++       Sigma_c*++
Alias Myanti-Sigma_c*--  anti-Sigma_c*--
ChargeConj MySigma_c*++  Myanti-Sigma_c*--
#
Alias      MyK*0                K*0
Alias      Myanti-K*0           anti-K*0
ChargeConj MyK*0                Myanti-K*0
#
Alias      MyDelta++            Delta++
Alias      Myanti-Delta--       anti-Delta--
ChargeConj MyDelta++            Myanti-Delta--
#
Alias      MyLambda(1520)0      Lambda(1520)0
Alias      Myanti-Lambda(1520)0 anti-Lambda(1520)0
ChargeConj MyLambda(1520)0      Myanti-Lambda(1520)0
#
Decay Lambda_b0sig
  0.500       MySigma_c++ MyD*- K-     PHSP;
  0.500       MySigma_c*++ MyD*- K-     PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MySigma_c++
  1.00      MyLambda_c+  pi+                   PHSP;
Enddecay
CDecay Myanti-Sigma_c-- 
#
Decay MySigma_c*++
  1.00      MyLambda_c+  pi+                   PHSP;
Enddecay
CDecay Myanti-Sigma_c*-- 
#
Decay MyLambda_c+
  0.17 MyDelta++ K-                      PHSP;
  0.21 Myanti-K*0        p+              PHSP;
  0.08 MyLambda(1520)0   pi+             PHSP;
  0.54 p+                K-      pi+     PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyK*0
  1.00 K+        pi-                     PHSP;
Enddecay
CDecay Myanti-K*0
#
Decay MyDelta++
  1.00 p+        pi+                     PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyLambda(1520)0
  1.00 p+        K-                      PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
Decay MyD*-
  1.0 Myanti-D0 pi- VSS;
Enddecay
CDecay MyD*+
#
Decay Myanti-D0
  1.0 K+ pi- PHSP;
Enddecay
CDecay MyD0
#
End
