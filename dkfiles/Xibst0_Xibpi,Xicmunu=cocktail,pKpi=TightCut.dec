# EventType: 16876010
# NickName: Xibst0_Xibpi,Xicmunu=cocktail,pKpi=TightCut
# Descriptor: [Sigma_b0 -> (Xi_b- -> Xi_c0 mu- anti_nu_mu) pi+ ]cc
#
# PhysicsWG: Onia
# 
# Cuts: LoKi::GenCutTool/TightCut

# Documentation: 
#    This is the decay file for the decay XibStar0 -> (Xi_b- -> Xi_c0 mu- anti_nu_mu ) pi+)
#  EndDocumentation
# 
# InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# 
#
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay     = '[Sigma_b0 ==> (Xi_b- ==> (Xi_c0 --> ^p+ ^K- ^K- ^pi+ {gamma} {gamma} {gamma} ) ^mu- [nu_mu]CC  {X} {X} {X} {X} {X} {X} {X} {X} )  pi+ {X} {X} {X} {X} {X} {X} {X} {X} ]CC'
# tightCut.Cuts      =    {
#     '[K-]cc'   : ' goodKaon ' ,
#     '[p+]cc'   : ' goodProton ' ,
#     '[pi+]cc'  : ' goodpi  ' ,
#     '[mu-]cc'  : ' goodmu  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import GeV',
#     'inAcc    = in_range ( 0.010 , GTHETA , 0.400 ) ' ,
#     'goodKaon = ( GPT > 0.24 * GeV ) & ( GP > 1.8 * GeV ) & inAcc ' ,
#     'goodProton = ( GPT > 0.24 * GeV ) & ( GP > 7.5 * GeV ) & inAcc ' ,
#     'goodpi  = ( GPT > 0.19 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodmu  = ( GPT > 0.9 * GeV ) & ( GP > 5.0 * GeV ) & inAcc ' ]
#
# EndInsertPythonCode
#
## ParticleValue: "Sigma_b0    112   5212 0.0 6.227  6.000000e-020  Sigma_b0    5212  1.000000e-004", "Sigma_b~0   113   -5212 0.0 6.227  6.000000e-020    anti-Sigma_b0   -5212  1.000000e-004"
#
# Email: sblusk@syr.edu, kkim10@syr.edu
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible:  Steve Blusk, Kyungeun Kim
# Date: 20190614

Alias MyXi_c*0 Xi_c*0
Alias Myanti-Xi_c*0 anti-Xi_c*0
ChargeConj MyXi_c*0 Myanti-Xi_c*0 

### Xi (2790)
Alias MyXi_c(2790) Xi_c(2790)0
Alias Myanti-Xi_c(2790) anti-Xi_c(2790)0
ChargeConj MyXi_c(2790) Myanti-Xi_c(2790)
### Xi (2815)
Alias MyXi_c(2815) Xi_c(2815)0
Alias Myanti-Xi_c(2815) anti-Xi_c(2815)0
ChargeConj MyXi_c(2815) Myanti-Xi_c(2815)

Alias MyXi'_c0 Xi'_c0 
Alias Myanti-Xi'_c0 anti-Xi'_c0
ChargeConj MyXi'_c0 Myanti-Xi'_c0 

Alias      MyXic0        Xi_c0
Alias      Myanti-Xic0   anti-Xi_c0
ChargeConj Myanti-Xic0   MyXic0

#
Alias      MyXib      Xi_b-
Alias      Myanti-Xib anti-Xi_b+
ChargeConj Myanti-Xib MyXib

Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
#
Alias      MyK_0*0    K_0*0
Alias      Myanti-K_0*0 anti-K_0*0
ChargeConj MyK_0*0   Myanti-K_0*0 
#


Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-

# Force to decay to Xib- pi+:
Decay Sigma_b0sig
1.000    MyXib        pi+     PHSP;
Enddecay
CDecay anti-Sigma_b0sig

Decay MyXib
  0.475     MyXic0   mu- anti-nu_mu                    PHSP;
  0.025     MyXic0   Mytau-  anti-nu_tau               PHSP;
  0.11875   MyXi'_c0  mu- anti-nu_mu                    PHSP;
  0.00625   MyXi'_c0  Mytau-  anti-nu_tau               PHSP;
  0.11875   MyXi_c*0  mu- anti-nu_mu                    PHSP;
  0.00625   MyXi_c*0  Mytau-  anti-nu_tau               PHSP;
  0.11875   MyXi_c(2790)   mu- anti-nu_mu                    PHSP;
  0.00625   MyXi_c(2790)   Mytau-  anti-nu_tau               PHSP;
  0.11875   MyXi_c(2815)   mu- anti-nu_mu                    PHSP;
  0.00625   MyXi_c(2815)   Mytau- anti-nu_tau                    PHSP;
Enddecay
CDecay Myanti-Xib

Decay MyXic0
  0.5   p+  K-     Myanti-K*0                           PHSP;
  0.5   p+   K-  K-  pi+                                 PHSP;
Enddecay
CDecay Myanti-Xic0

Decay MyXi'_c0
1.0    gamma  MyXic0                    PHSP;
Enddecay
CDecay Myanti-Xi'_c0

Decay MyXi_c*0
0.5    gamma  MyXic0                    PHSP;
0.5    pi0    MyXic0                    PHSP;
Enddecay
CDecay Myanti-Xi_c*0

Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyXi_c(2790)
0.5	pi0	MyXic0			PHSP;
0.5	pi0	MyXi'_c0		PHSP;
Enddecay
CDecay Myanti-Xi_c(2790)

Decay MyXi_c(2815)
1.0	pi-	pi+	MyXic0		PHSP;
Enddecay
CDecay Myanti-Xi_c(2815)
#
Decay Mytau-
1.0     mu-  anti-nu_mu   nu_tau        TAULNUNU;
Enddecay
CDecay Mytau+

End

