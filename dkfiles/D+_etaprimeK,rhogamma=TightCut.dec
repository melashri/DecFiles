# EventType: 21103220
#
# Descriptor: [D+ => ( eta' => ( rho0 => pi+ pi- ) gamma) K+]cc
#
# NickName: D+_etaprimeK,rhogamma=TightCut
#
#                                                                          
# Cuts: LoKi::GenCutTool/TightCut                                          
#                                                                          
# InsertPythonCode:                                                        
#                                                                          
# from Configurables import LoKi__GenCutTool                               
# gen = Generation()                                                       
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )                
# tightCut = gen.SignalPlain.TightCut                         
# tightCut.Decay     = '^[ D+ => ( eta_prime => ( rho(770)0 => ^pi+ ^pi- ) ^gamma ) ^K+]CC'    
# tightCut.Cuts      =    {                                   
#     '[pi+]cc'    : ' inAcc & dauCuts',                      
#     '[K+]cc'    : ' inAcc & dauCuts',                      
#     '[D+]cc'   : 'Dcuts' }                                
# tightCut.Preambulo += [                                     
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' ,          
#     'dauCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',   
#     'Dcuts = (GPT > 1000 * MeV)' ]  
# EndInsertPythonCode   
#   
#   
#
# Documentation: Forces a D+ to ( eta_prime => (rho0 => pi+ pi-) gamma ) K+ with generator level cuts 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Simone Stracka
# Email: simone.stracka@cern.ch
# Date: 20190527
#
Alias       my_eta'   eta'
ChargeConj  my_eta'   my_eta'
Alias       my_rho0   rho0
ChargeConj  my_rho0   my_rho0
#
Decay  D+sig
  1.000     my_eta'   K+    PHOTOS PHSP ;
Enddecay
CDecay D-sig
#
Decay  my_eta'
  1.000     my_rho0    gamma                                   SVP_HELAMP  1.0 0.0 1.0 0.0;
Enddecay
#
Decay my_rho0
1.000    pi+ pi-                       PHOTOS   VSS;
Enddecay

End
