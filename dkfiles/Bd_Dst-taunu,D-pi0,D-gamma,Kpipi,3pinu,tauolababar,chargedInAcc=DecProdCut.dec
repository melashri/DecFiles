# EventType: 11563410
#
# Descriptor: {[[B0]nos ->(D*- -> {pi0, gamma} (D- -> K+ pi- pi- )) (tau+ -> pi+ pi- pi+ anti-nu_tau) nu_tau]cc, [[B0]os -> (D*+ -> {pi0,gamma} (D+ -> K- pi+ pi+)) (tau- -> pi- pi+ pi- nu_tau) anti-nu_tau]cc}
#
# NickName: Bd_Dst-taunu,D-pi0,D-gamma,Kpipi,3pinu,tauolababar,chargedInAcc=DecProdCut
#
# Cuts: DaughtersInLHCb
# CutsOptions: NeutralThetaMin 0. NeutralThetaMax 10.
# 
# Documentation: D*- -> D- {pi0,gamma} , D- -> K pi pi. The {pi0,gamma} are not forced
# to be in the LHCb acceptance.
# Tau lepton decays in 3pi nu mode using the Tauola BaBar model.
# EndDocumentation
#
# PhysicsWG: B2SL
#
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Beatriz Garcia Plana, Antonio Romero Vidal
# Email: beatriz.garcia.plana@cern.ch, antonio.romero@usc.es
# Date: 20180202
#
# Tauola steering options
# The following forces the tau to decay into 3 charged pions (not pi+2pi0)
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias         MyD-              D-
Alias         MyD+              D+
ChargeConj    MyD-              MyD+
#
Alias         MyD*-             D*-
Alias         MyD*+             D*+
ChargeConj    MyD*-             MyD*+
#
Alias         MyTau+    	tau+
Alias         MyTau-     	tau-
ChargeConj    MyTau+     	MyTau-
#
Decay B0sig
  1.000       MyD*-        MyTau+   nu_tau   ISGW2;
Enddecay
CDecay anti-B0sig
#
Decay MyD*-
# BR(D*- -> D- pi0)   = 30.7% (PDG2017)
# BR(D*- -> D- gamma) = 1.6% (PDG201)
  0.950  MyD-   pi0                VSS;
  0.050  MyD-   gamma              VSP_PWAVE;
Enddecay
CDecay MyD*+
#
Decay MyD-
# D_DALITZ includes resonances contributions (K*(892), K*(1430), K*(1680))
  1.000    K+    pi-    pi-          D_DALITZ;
Enddecay
CDecay MyD+
#    
Decay MyTau-
  1.0000	TAUOLA 5;
Enddecay
CDecay MyTau+
#   
End
#
