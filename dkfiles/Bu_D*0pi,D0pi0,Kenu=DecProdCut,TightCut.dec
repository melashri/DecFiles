# EventType: 12583425
#
# Descriptor: [B+ -> (D*(2007)~0 -> (D~0 -> K+ e- anti-nu_e) pi0) pi+]cc
#
# NickName: Bu_D*0pi,D0pi0,Kenu=DecProdCut,TightCut
#
# Documentation: D chain background for B+ -> Kee for RK at high q2. m(ee) > 3674 MeV, pt > 200 MeV
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Alex Marshall
# Email: alex.marshall@cern.ch
# Date: 20210518
# CPUTime: 2 min
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool( LoKi__GenCutTool,'HighVisMass')
# evtgendecay.HighVisMass.Decay = "[^(B+ ==> (D*(2007)~0 ==> (D~0 ==> K+ e- nu_e~) pi0) pi+)]CC"
# evtgendecay.HighVisMass.Preambulo += [
#     "CS  = LoKi.GenChild.Selector",
#     "massCut = GMASS ( CS('[(B+ ==> (D*(2007)~0 ==> (D~0 ==> K+ ^e- nu_e~) pi0) pi+)]CC'), CS('[(B+ ==> (D*(2007)~0 ==> (D~0 ==> K+ e- nu_e~) pi0) ^pi+)]CC') ) > 3674 * MeV"
# ]
# evtgendecay.HighVisMass.Cuts = {
#     '[B+]cc'             : " massCut "
# }
#
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
#
# tightCut.Decay     = "[(B+ ==> (D*(2007)~0 ==> (D~0 ==> ^K+ ^e- nu_e~) pi0) ^pi+)]CC"
#
# tightCut.Preambulo += [
#     "from LoKiCore.functions import in_range",
#     "from GaudiKernel.SystemOfUnits import GeV, MeV",
#     "inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) "]
#
# tightCut.Cuts      =    {
#     '[e-]cc'             : " inAcc & ( GPT > 200 * MeV )  " ,
#     '[K+]cc'             : " inAcc & ( GPT > 200 * MeV )  " ,
#     '[pi+]cc'             : " inAcc & ( GPT > 200 * MeV )  " 
# }
#
# EndInsertPythonCode
#
#
Alias           My_D0           D0
Alias           My_anti-D0      anti-D0
ChargeConj      My_D0           My_anti-D0
Alias           My_D*0           D*0
Alias           My_anti-D*0      anti-D*0
ChargeConj      My_D*0           My_anti-D*0
#
Decay B+sig
1.000        My_anti-D*0     pi+               PHSP;
Enddecay
CDecay B-sig
#
#
Decay My_anti-D*0 
1.000        My_anti-D0     pi0              PHSP;
Enddecay
CDecay My_D*0
#
#
Decay My_anti-D0
1.000        K+        e-      anti-nu_e           PHOTOS ISGW2;
Enddecay
CDecay My_D0
#	
End
#

