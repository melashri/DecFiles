# EventType: 15574002
# 
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> p+ K- pi+)  anti-nu_mu mu-]cc
# 
# NickName: Lb_Lcmunu,pKpi=DecProdCut,HighVisMass,EvtGenCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(Lambda_b0 => (Lambda_c+ => p+ K- pi+) mu- nu_mu~)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[Lambda_b0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += ["visMass  = ( ( GMASS ( 'mu-' == GABSID , 'pi+' == GABSID, 'p+' == GABSID, 'K-' == GABSID ) ) > 4500 * MeV ) " ]
#
# EndInsertPythonCode
#
# Documentation: Adapted from "Lb_Lcmunu,L0enu=DecProdCut,HighVisMass,EvtGenCut.dec".
# Semi-leptonic Lambda B decay into Lc mu nu. Lc decays to p+ K- pi+.
# Generator level cut applied to have a visible mass larger than 4.5 GeV.
# Semileptonic background for Lb->Lambda(1520)emu.
# EndDocumentation
#
# CPUTime: <1 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Dan Thompson   
# Email: dan.thompson@cern.ch
# Date: 20211125
#
#
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
###
Decay Lambda_b0sig
  1.000    MyLambda_c+        mu-  anti-nu_mu     PHOTOS   BaryonPCR  1 1 1 1;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
  1.000   p+ K- pi+         PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
#
End
#
