# EventType: 15584001
# 
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> p+ K- e+ nu_e)  pi-]cc
# 
# NickName: Lb_Lcpi,pKenu=DecProdCut,HighVisMass,EvtGenCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(Lambda_b0 => (Lambda_c+ => p+ K- e+ nu_e) pi-)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[Lambda_b0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += ["visMass  = ( ( GMASS ( 'pi-' == GABSID , 'e+' == GABSID, 'p+' == GABSID, 'K-' == GABSID ) ) > 4500 * MeV ) " ]
#
# EndInsertPythonCode
#
# Documentation: Adapted from "Lb_Lcmunu,L0enu=DecProdCut,HighVisMass,EvtGenCut.dec".
# Semi-leptonic Lambda B decay into Lc pi. Lc decays to p+ K- e+ nu.
# Generator level cut applied to have a visible mass larger than 4.5 GeV.
# Semileptonic background for Lb->Lambda(1520)emu.
# EndDocumentation
#
# CPUTime: <1 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Dan Thompson   
# Email: dan.thompson@cern.ch
# Date: 20210316
#
#
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
###
Decay Lambda_b0sig
  1.000    MyLambda_c+        pi-     PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
  1.000   p+ K- e+ nu_e          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
#
End
#
