# EventType: 11444410
#
# Descriptor: [B0 -> (psi(2S) -> mu+ mu-) (K*(1410)0 -> (K*(892)0 -> K+ pi-) pi0)]cc
#
# NickName: Bd_Psi2SKX=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: B0 -> psi(2S) K+ X, psi(2S) -> mu+ mu-.
#     Decays include several intermediate K*0, but exclude
#     B0 -> psi(2S) K+ pi- final states.
#     Relative proportion of K* resonance components is
#     taken from Belle paper arXiv:1306.4894v3 and arXiv:1306.4894.
#     Included intermediate state:
#     K*(892)+, K*(1410)+, K2*(1430)+, K*(1680)+, K_1(1270)+ resonances
#     also non-resonant mode and decay with eta and omega.
#     The decay modes are taken from PDG.
#     Fractions of the decays are taken as a branching fraction, but then
#     scaled to make sum of fractions of all the final states to be 1.
#
#     Tight generator level cuts applied for muons and charged kaon in the
#     final state, which increases the statistics with the factor of ~1.5:
#     Efficiency w/  tight cuts    in output           (19.92 +- 0.56)%
#                                  in GeneratorLog.xml ( 9.80 +- 0.42)%
#     Efficiency w/o tight cuts    in output           (27.86 +- 0.75)%
#                                  in GeneratorLog.xml (13.68 +- 0.57)%
# EndDocumentation
#
# Sample: SignalRepeatedHadronization
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalRepeatedHadronization 
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut   = signal.TightCut
# tightCut.Decay = '^[(B0|B~0) --> (psi(2S) => mu+ mu-) K+ ...]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV                      ',
#     'inAcc        = in_range ( 0.005 , GTHETA , 0.400 )                                          ',
#     'inEta        = in_range ( 1.95  , GETA   , 5.050 )                                          ',
#     'inY          = in_range ( 1.9   , GY     , 4.6   )                                          ',
#     'fastTrack    = ( GPT > 180 * MeV ) & in_range( 2.9 * GeV, GP, 210 * GeV )                   ',
#     'goodK        = inAcc & inEta & fastTrack                                                    ',
#     'goodMu       = inAcc & inEta & fastTrack & (GPT > 500 * MeV)                                ',
#     'longLived    = 75 * micrometer < GTIME                                                      ',
#     'goodPsi      = inY & GINTREE( ("mu+" == GID) & goodMu ) & GINTREE( ("mu-" == GID) & goodMu )',
#     'goodBDaugPsi = GINTREE ( ("psi(2S)" == GABSID) & goodPsi )                                  ',
#     'goodBDaugK   = GINTREE ( ("K+"      == GABSID) & goodK   )                                  ',
#     'goodB        = inY & longLived & goodBDaugPsi & goodBDaugK                                  ',
# ]
# tightCut.Cuts = {
#     '[B0]cc': 'goodB',
# }
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Slava Matiunin
# Email: Viacheslav.Matiunin@<no-spam>cern.ch
# Date: 20211105
# CPUTime: <1 min
#
Define PKHplus 0.159
Define PKHzero 0.775
Define PKHminus 0.612
Define PKphHplus 1.563
Define PKphHzero 0.000
Define PKphHminus 2.712
#
Alias      Mypsi(2S) psi(2S)
ChargeConj Mypsi(2S) Mypsi(2S)
#
## K*(892)0
Alias      My2K*0      K*0
Alias      My2anti-K*0 anti-K*0
ChargeConj My2K*0      My2anti-K*0
#
## K*(892)+
Alias      My2K*+ K*+
Alias      My2K*- K*-
ChargeConj My2K*+ My2K*-
#
## K*(1410)0
Alias      My3K*0      K'*0
Alias      My3anti-K*0 anti-K'*0
ChargeConj My3K*0      My3anti-K*0
#
## K2*(1430)0
Alias      My4K*0      K_2*0
Alias      My4anti-K*0 anti-K_2*0
ChargeConj My4K*0      My4anti-K*0
#
## K*(1680)0
Alias      My5K*0      K''*0
Alias      My5anti-K*0 anti-K''*0
ChargeConj My5K*0      My5anti-K*0
#
## K0*(1430)+
Alias      My6K*+ K_0*+
Alias      My6K*- K_0*-
ChargeConj My6K*+ My6K*-
#
## K0*(1430)0
Alias      My6K*0      K_0*0
Alias      My6anti-K*0 anti-K_0*0
ChargeConj My6K*0      My6anti-K*0
#
## K_1(1270)0
Alias      MyK_10      K_10
Alias      Myanti-K_10 anti-K_10
ChargeConj MyK_10      Myanti-K_10
#
Decay B0sig
    ## resonances
    0.0209  Mypsi(2S) K+     pi-   pi0  PHSP ;
    0.5196  Mypsi(2S) My3K*0            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.0343  Mypsi(2S) My4K*0            PHSP ;
    0.0651  Mypsi(2S) My5K*0            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.2984  Mypsi(2S) MyK_10            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.0617  Mypsi(2S) My2K*0 eta        PHSP ;
Enddecay
CDecay anti-B0sig
#
Decay Mypsi(2S)
    1.0000  mu+       mu-         PHOTOS VLL ;
Enddecay
#
## K*(892)0
Decay My2K*0
    1.0000  K+        pi-         VSS ;
Enddecay
CDecay My2anti-K*0
#
## K*(892)+
Decay My2K*+
    1.0000  K+        pi0         VSS ;
Enddecay
CDecay My2K*-
#
## K*(1410)0
Decay My3K*0
    0.1077  rho-      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.4462  My2K*+    pi-         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.4462  My2K*0    pi0         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
CDecay My3anti-K*0
#
## K2*(1430)0
Decay My4K*0
    0.2551  rho-      K+          TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.2414  My2K*+    pi-         TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.2414  My2K*0    pi0         TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.1310  My2K*+    pi-    pi0  PHSP ;
    0.1310  My2K*0    pi+    pi-  PHSP ;
Enddecay
CDecay My4anti-K*0
#
## K*(1680)0
Decay My5K*0
    0.6117  rho-      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.1942  My2K*+    pi-         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.1942  My2K*0    pi0         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
CDecay My5anti-K*0
#
## K_1(1270)0
Decay MyK_10
    0.5746  rho-      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.0730  My2K*+    pi-         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.0730  My2K*0    pi0         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.1517  My6K*+    pi-         VSS;
    0.1277  My6K*0    pi0         VSS;
Enddecay
CDecay Myanti-K_10
#
## K0*(1430)+
Decay My6K*+
    0.7828  K+       pi0          PHSP ;
    0.2172  eta      K+           PHSP ;
Enddecay
CDecay My6K*-
#
## K0*(1430)0
Decay My6K*0
    1.0000  K+       pi-          PHSP ;
Enddecay
CDecay My6anti-K*0
#
End
#
