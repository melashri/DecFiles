# EventType: 12813402
#
# Descriptor: [B- -> p+ (anti-N(1440)- -> anti-p- pi0) mu- anti-nu_mu]cc
#
# NickName: Bu_pNstmunu,pX=TightCutpQCD
#
# Cuts: LoKi::GenCutTool/TightCut
# 
# CPUTime: < 1 min
#
# Documentation: B-> p Nst mu nu_mu cocktail using pQCD model, N* decayed using PHSP, decay products in LHCb acceptance. Relative B -> p Nst mu nu_mu branching fractions calculated according to four-body phase space factor. For more detials contact Ryan Newcombe. N* decay Branching Fractions taken from Bu_pNstmunu,pX=TightCut.dec
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut  = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[ B- ==> ^p+  ^p~-  ^mu- nu_mu~ {X} {X} {X} {X}  ]CC"
# tightCut.Preambulo += [
# "from LoKiCore.functions import in_range"  ,
# "from GaudiKernel.SystemOfUnits import GeV, MeV",
#  ]
# tightCut.Cuts      =    {
#'[p+]cc'   : " in_range( 0.010 , GTHETA , 0.400 )& ( GPT > 750 * MeV ) & (GP > 14600 * MeV)" ,
#'[mu-]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) & (GP > 2900 * MeV) "
#   }
#
# from Configurables import LHCb__ParticlePropertySvc
# LHCb__ParticlePropertySvc().Particles = [ 
# "N(1440)+              636       12212   1.0      1.4400000      2.194041e-24                 N(1440)+           21440      0.00",
# "N(1440)~-             637      -12212  -1.0      1.4400000      2.194841e-24                   anti-N(1440)-           -21440      0.00",
# "N(1520)+              420        2124   1.0      1.52000000      5.723584e-24                   N(1520)+           21520      0.00",
# "N(1520)~-             421       -2124  -1.0      1.52000000     5.723584e-24                   anti-N(1520)-           -21520      0.00",
# "N(1535)+              713       22212   1.0      1.53500000      4.388081e-24                   N(1535)+           21535      0.00",
# "N(1535)~-             714      -22212  -1.0      1.53500000      4.388081e-24                   anti-N(1535)-           -21535      0.00",
# "N(1720)+              775       32124   1.0      1.72000000      2.632849e-24                   N(1720)+           21720      0.00",
# "N(1720)~-             776      -32124  -1.0      1.72000000      2.632849e-24                   anti-N(1720)-           -21720      0.00"
# ]
# EndInsertPythonCode
#
#
# PhysicsWG: B2SL 
# Tested: Yes
# Responsible: Ryan Newcombe
# Email: ryan.newcombe@cern.ch
# Date: 20191129 
#
#
Alias  MyDelta+  Delta+
Alias  Myanti-Delta-  anti-Delta-
ChargeConj MyDelta+  Myanti-Delta-

Alias  MyDelta0  Delta0
Alias  Myanti-Delta0  anti-Delta0
ChargeConj MyDelta0  Myanti-Delta0

Alias  MyN(1440)+  N(1440)+
Alias  Myanti-N(1440)-  anti-N(1440)-
ChargeConj MyN(1440)+  Myanti-N(1440)-

Alias  MyDelta++  Delta++
Alias  Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++  Myanti-Delta--

Alias  MyN(1535)+  N(1535)+
Alias  Myanti-N(1535)-  anti-N(1535)-
ChargeConj MyN(1535)+  Myanti-N(1535)-

Alias  MyN(1520)+  N(1520)+
Alias  Myanti-N(1520)-  anti-N(1520)-
ChargeConj MyN(1520)+  Myanti-N(1520)-

Alias  MyN(1720)+  N(1720)+
Alias  Myanti-N(1720)-  anti-N(1720)-
ChargeConj MyN(1720)+  Myanti-N(1720)-
#
Decay B+sig
 1.0000  anti-p-  MyN(1440)+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 300000;
 0.8951  anti-p-  MyN(1520)+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 800000;
 0.8762  anti-p-  MyN(1535)+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 200000;
 0.6645  anti-p-  MyN(1720)+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 600000;
 1.0000  Myanti-N(1440)-  p+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 300000;
 0.8951  Myanti-N(1520)-  p+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 800000;
 0.8762  Myanti-N(1535)-  p+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 200000;
 0.6645  Myanti-N(1720)-  p+  mu+  nu_mu  PHOTOS  BToDiBaryonlnupQCD  67.7  -280.0  -38.3  -840.1  -10.1  -157.0 600000;
Enddecay
CDecay B-sig
#
Decay MyN(1440)+
 0.217 p+ pi0  PHOTOS PHSP;
 0.123 MyDelta++ pi-  PHOTOS PHSP;
 0.056 MyDelta+ pi0  PHOTOS PHSP;
 0.0139 MyDelta0 pi+  PHOTOS PHSP;
 0.067 p+ pi+ pi-  PHOTOS PHSP;
 0.033 p+ pi0 pi0  PHOTOS PHSP;
 0.00415 p+ gamma  PHOTOS PHSP;
Enddecay
CDecay Myanti-N(1440)-
#
Decay MyN(1520)+
 0.20  p+  pi0  PHOTOS PHSP;
 0.10  MyDelta++ pi-  PHOTOS PHSP;
 0.044 MyDelta+  pi0  PHOTOS PHSP;
 0.011 MyDelta0  pi+  PHOTOS PHSP;
 0.20  p+  rho0  PHOTOS PHSP;
 0.004 p+  gamma  PHOTOS PHSP;
 0.0023 p+ eta  PHOTOS PHSP;
Enddecay
CDecay Myanti-N(1520)-
#
Decay MyN(1535)+
 0.15  p+  pi0  PHOTOS PHSP;
 0.42  p+  eta  PHOTOS PHSP;
 0.0133 p+ pi- pi+ PHOTOS PHSP;
 0.0067 p+ pi0 pi0 PHOTOS PHSP;
 0.0267  MyN(1440)+  pi0 PHOTOS PHSP;
 0.00225 p+  gamma PHOTOS PHSP;
Enddecay
CDecay Myanti-N(1535)-
#
Decay MyN(1720)+
 0.037  p+  pi0  PHOTOS PHSP;
 0.75  p+  rho0  PHOTOS PHSP;
 0.14  MyDelta+  pi0 PHOTOS PHSP;
 0.04  p+  eta PHOTOS PHSP;
 0.0015  p+ gamma  PHOTOS PHSP;
Enddecay
CDecay Myanti-N(1720)-
#
Decay MyDelta++
 1.0 p+  pi+ PHOTOS  PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyDelta+
 1.0 p+  pi0 PHOTOS  PHSP;
Enddecay
CDecay Myanti-Delta-
#
Decay MyDelta0
 1.0 p+  pi- PHOTOS  PHSP;
Enddecay
CDecay Myanti-Delta0
#
End
#
