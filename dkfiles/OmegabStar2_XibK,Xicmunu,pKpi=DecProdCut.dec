# EventType: 16875002
# NickName: OmegabStar2_XibK,Xicmunu,pKpi=DecProdCut
# Descriptor: [Sigma_b- -> K- (Xi_b0 -> (Xi_c+ -> p+ K- pi+) mu- anti-nu_mu)]cc
#
# Documentation:
#   Decay Omega_b**- -> Xib0 K- with Xib0 -> Xic+ mu- nu and Xic+ --> p K- pi+
#   Set deltaM=M(Xib**)-M(Lb0)-M(K) to be 116 MeV, as seen in data
#   Daughters in LHCb Acceptance
# EndDocumentation
#
# Cuts: LHCbAcceptance
#
# ParticleValue: " Sigma_b-   114   5112 -1.0  6.340  6.000000e-020       Sigma_b-   5112  1.000000e-004", " Sigma_b~+  115  -5112  1.0  6.340  6.000000e-020  anti-Sigma_b+  -5112  1.000000e-004", " Xi_b0   124   5232  0.0  5.7918  1.480000e-012       Xi_b0   5232  0.000000e+000", " Xi_b~0  125  -5232  0.0  5.7918  1.480000e-012  anti-Xi_b0  -5232  0.000000e+000"
#
# Email: sblusk@syr.edu
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: <1min
# Responsible: Steve Blusk
# Date: 20170512
#

Alias MyXi_c+ Xi_c+
Alias Myanti-Xi_c- anti-Xi_c-
ChargeConj MyXi_c+ Myanti-Xi_c-
#
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
#
Alias      MyK_0*0    K_0*0
Alias      Myanti-K_0*0 anti-K_0*0
ChargeConj MyK_0*0   Myanti-K_0*0 
#

Alias      MyDelta++  Delta++
Alias      Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++  Myanti-Delta--

Alias MyLambda(1690)0 Lambda(1690)0
Alias Myanti-Lambda(1690)0 anti-Lambda(1690)0
ChargeConj  MyLambda(1690)0 Myanti-Lambda(1690)0

Alias MyLambda(1520)0 Lambda(1520)0
Alias Myanti-Lambda(1520)0 anti-Lambda(1520)0
ChargeConj  MyLambda(1520)0 Myanti-Lambda(1520)0

Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-
#
Alias MyXi'_c+ Xi'_c+ 
Alias Myanti-Xi'_c- anti-Xi'_c-
ChargeConj MyXi'_c+ Myanti-Xi'_c- 

Alias MyXi_c*+ Xi_c*+
Alias Myanti-Xi_c*- anti-Xi_c*-
ChargeConj MyXi_c*+ Myanti-Xi_c*- 

Alias MyXi_b0       Xi_b0
Alias Myanti-Xi_b0  anti-Xi_b0
ChargeConj MyXi_b0  Myanti-Xi_b0


# Force Sb- (stand-in for Xib**-) to decay to Xib0 pi-:
Decay Sigma_b-sig
1.000    MyXi_b0         K-     PHSP;
Enddecay
CDecay anti-Sigma_b+sig

Decay MyXi_b0
  0.546     MyXi_c+   mu- anti-nu_mu                    PHSP;
  0.035     MyXi_c+   Mytau-  anti-nu_tau               PHSP;
  0.0546    MyXi'_c+  mu- anti-nu_mu                    PHSP;
  0.0035    MyXi'_c+  Mytau-  anti-nu_tau               PHSP;
  0.0546    MyXi_c*+  mu- anti-nu_mu                    PHSP;
  0.0035    MyXi_c*+  Mytau-  anti-nu_tau               PHSP;
Enddecay
CDecay Myanti-Xi_b0

Decay MyXi_c+
  0.12   MyDelta++ K-                                      PHSP;
  0.40   p+      Myanti-K*0                              PHSP;
  0.25   p+      Myanti-K_0*0                            PHSP; 
  0.13   MyLambda(1690)0 pi+                             PHSP;
  0.02   MyLambda(1520)0 pi+                             PHSP;
  0.08   p+   K-  pi+                                    PHSP;
Enddecay
CDecay Myanti-Xi_c-
#
Decay MyXi'_c+
1.0    gamma  MyXi_c+                    PHSP;
Enddecay
CDecay Myanti-Xi'_c-

Decay MyXi_c*+
0.5    gamma  MyXi_c+                    PHSP;
0.5    pi0    MyXi_c+                    PHSP;
Enddecay
CDecay Myanti-Xi_c*-

Decay MyDelta++
1.0000    p+  pi+                     PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyK_0*0
  1.000 K+ pi-                    PHSP;
Enddecay
CDecay Myanti-K_0*0
#
#
Decay MyLambda(1690)0
  1.0      p+  K-               PHSP;
Enddecay
CDecay Myanti-Lambda(1690)0

Decay MyLambda(1520)0
  1.0      p+  K-               PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0

#
Decay Mytau-
  0.1736       mu-  anti-nu_mu   nu_tau        TAULNUNU;
Enddecay
CDecay Mytau+

End



