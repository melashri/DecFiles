# EventType: 13596242
#
# Descriptor: {[[B_s0]nos -> (D_s- -> (phi(1020) -> K+ K-) mu- anti-nu_mu) (Ds+ -> (phi(1020) -> K+ K-) mu+ nu_mu)]cc, [[B_s0]os -> (D_s+ -> (phi(1020) -> K+ K-) mu+ nu_mu) (Ds- -> (phi(1020) -> K+ K-) mu- anti-nu_mu)]cc}
#
# NickName: Bs_DsDs,phimunu,phimunu=KKmumuInAcc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# kkmumuInAcc = Generation().SignalRepeatedHadronization.TightCut
# kkmumuInAcc.Decay = '[^(B_s0 ==> K+ K- ^mu+ ^mu- nu_mu nu_mu~ {X} {X} {X} {X})]CC'
# kkmumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.01, GTHETA, 0.400))',
#     'twoKaonsInAcc = (GNINTREE( ("K+"==GID) & inAcc) >= 1) & (GNINTREE( ("K-"==GID) & inAcc) >= 1)'
#     ]
# kkmumuInAcc.Cuts = {
#     '[mu+]cc'   : 'inAcc',
#     '[B_s0]cc'   : 'twoKaonsInAcc'
#     }
#
# EndInsertPythonCode
#
# Documentation: Bs -> Ds Ds decays, where both Ds decay to phi mu nu, with KKmumu in acceptance
# EndDocumentation
#
# CPUTime: < 1 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20210811
#
Alias 		MyD_s+		D_s+
Alias		MyD_s-		D_s-
ChargeConj	MyD_s+		MyD_s-
#
Alias 		MyD_s*+ 	D_s*+
Alias		MyD_s*-	        D_s*-
ChargeConj	MyD_s*+	        MyD_s*-
#
Alias           MyPhi           phi
ChargeConj      MyPhi           MyPhi
#
Decay B_s0sig  
  0.0044   	MyD_s-       	MyD_s+	     	PHSP;
  0.0070   	MyD_s*+     	MyD_s-      	SVS;
  0.0070  	MyD_s*-     	MyD_s+      	SVS;
  0.0144  	MyD_s*-         MyD_s*+     	SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0; 
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s+
  1.000         MyPhi           mu+   nu_mu     ISGW2;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
  0.935		MyD_s+          gamma           VSP_PWAVE;
  0.058         MyD_s+          pi0		VSS;
Enddecay
CDecay MyD_s*-
#
Decay MyPhi
  1.000         K+              K-              VSS;
Enddecay
#
End
#

