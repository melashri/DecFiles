# EventType: 13104323  
#
# Descriptor: [Beauty -> K+ pi- (KS0 -> pi+ pi-) gamma]cc
#
# NickName: Bs_KsK+pi-gamma=TightCut,mKshhCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = '[^(Beauty => K+ pi- KS0 gamma)]CC'
# evtgendecay.mKshhCut.Cuts  = {'[B_s0]cc' : ' (mKshhCut) '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = (GMASS(CS('[(Beauty => ^K+ pi- KS0 gamma)]CC'),CS('[(Beauty => K+ ^pi- KS0 gamma)]CC'), CS('[(Beauty => K+ pi- ^KS0 gamma)]CC')) < 2.3 * GeV)"]
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[Beauty => ^K+ ^pi- (KS0 => ^pi+ ^pi-) ^gamma]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'        : ' inAcc' , 
#     '[K+]cc'         : ' inAcc' , 
#     'gamma'          : ' goodPhoton'}
#
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter',
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'goodPhoton = ( GPT > 1.5 * GeV ) & inAcc' ]
#
# EndInsertPythonCode
#
# Documentation: Bkgd for Kpipipig, all in PHSP, in acceptance and m(KsKpi)< 2.3 GeV, with gamma PT > 1.5
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20190106
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Decay B_s0sig
  1.000   K+  pi-    MyK0s      gamma         PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyK0s
  1.000   pi+         pi-       PHSP;
Enddecay
#
End
