# EventType: 35103000
#
# Descriptor: [Xi- -> p+ pi- pi-]cc
#
# NickName: Xi-_ppipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: [Xi -> p+ pi- pi-]cc tight generator cut
#  * Lambda0 endvertex z in [-1m,0.8m]
#  * Lambda0 endvertex radial cut at 38mm
# EndDocumentation
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[^(Xi- => ^p+ ^pi- ^pi-)]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV" ,
#     "GY           =  LoKi.GenParticles.Rapidity () ## to be sure " , 
#     "inY          =  in_range ( 1.9   , GY     , 4.6   )         " ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         " ,
#     "inEta        =  in_range ( 1.95  , GETA   , 5.050 )         " , 
#     "goodTrack    =  inAcc & inEta" ,
#     "GVX = LoKi.GenVertices.PositionX() " ,
#     "GVY = LoKi.GenVertices.PositionY() " ,
#     "GVZ = LoKi.GenVertices.PositionZ() " ,
#     "vx    = GFAEVX ( GVX, 100 * meter )  " ,    
#     "vy    = GFAEVX ( GVY, 100 * meter )  " ,
#     "rho2  = vx**2 + vy**2 " ,
#     "rhoK  = rho2 < (38 * millimeter )**2  " , 
#     "decay = in_range ( -1 * meter, GFAEVX ( GVZ, 100 * meter ), 0.8 * meter ) ",
# ]
# tightCut.Cuts      =    {
#     "[Xi-]cc"  : "decay & rhoK", 
#    "[pi-]cc"        : "goodTrack " , 
#    "[p+]cc"         : "goodTrack "
#                         }
# EndInsertPythonCode
#
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Alexandre Brea Rodriguez
# Email: alexandre.brea.rodriguez@cern.ch
# Date: 20180731
#
#Alias      MyXi-          Xi-
#Alias      Myanti-Xi+     anti-Xi+
#ChargeConj MyXi-          Myanti-Xi+
Decay Xi-sig
  1.000       p+ pi- pi-    PHSP;
Enddecay

CDecay anti-Xi+sig
#
End
