# EventType: 27163476
# 
# Descriptor: { [D*+ -> (D0 -> (phi(1020) -> K+ K-) (eta -> gamma gamma) ) pi+]cc}
#
# NickName: Dst_D0pi,KKeta=TightCut,tighter
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#   Decay file for D* -> D0 pi+
#   where D0 decays to mode phi(->K+K-)eta 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Jolanta Brodzicka
# Email: Jolanta.Brodzicka@cern.ch
# Date: 20220110
# CPUTime: <1min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ D*(2010)+ => ^( D0 => (phi(1020) => K+ K-) ( eta -> gamma gamma ) ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV     ',
#     'inAcc       = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) )',
#     'goodD0Eta   = ( GINTREE( ("gamma"==GABSID) & (GPT > 1200 * MeV) & inAcc & inCaloAcc ) )',
#     'goodD0Km   = ( ("K-"==GABSID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0Kp   = ( ("K+"==GABSID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0      = ( (GPT > 1600 * MeV) & GINTREE(goodD0Km) & GINTREE(goodD0Kp) & GINTREE(goodD0Eta) )'
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'  : 'inAcc ',
#     '[D0]cc'   : 'goodD0 '
#     }
# EndInsertPythonCode

Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
#
Alias      Myphi   phi
ChargeConj Myphi   Myphi
#
Alias      Myeta  eta
ChargeConj Myeta Myeta
#
Decay D*+sig
1.0 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig
#
Decay MyD0
1.0   Myphi Myeta  SVS;
Enddecay
CDecay MyantiD0
#
Decay Myphi
1.0    K+    K-    VSS;
Enddecay
#
Decay Myeta
1.0     gamma gamma      PHSP;
Enddecay
#
End
 
