# EventType: 16145135
#
# Descriptor: [Xi_b- -> (Xi- -> (Lambda0 -> p+ pi-) pi-) (J/psi(1S) -> mu+ mu-)]cc
#
# NickName: Xib_JpsiXi,mm,Lambdapi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: J/psi forced to go to mu mu,  Xi forced to go Lambda pi (physical model), Lambda forced to go to p pi (physical model).
# Tight generator cuts on the Xi and its decay products.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[(Xi_b- => ^(Xi- => ^(Lambda0 => ^p+ ^pi-) ^pi-) (J/psi(1S) => ^mu+ ^mu-))]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, GeV" ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )     " ,
#     "inAccH       =  in_range ( 0.001 , GTHETA , 0.390 )     " ,
#     "inEtaL       =  in_range ( 1.5  , GETA   , 5.5 )        " , 
#     "inEtaLD      =  in_range ( 1.5  , GETA   , 7.5 )        " ,
#     "inP_pi       =  ( GP > 1.  *  GeV ) ",
#     "inP_p        =  ( GP > 3.5 *  GeV ) ",
#     "inP_mu       =  ( GP > 2.  *  GeV ) ",
#     "goodMuon     =  inAcc & inP_mu & inEtaL  " ,
#     "goodPion     =  inAccH & inP_pi & inEtaLD" ,
#     "goodProton   =  inAccH & inP_p  & inEtaLD" ,
#     "GVZ = LoKi.GenVertices.PositionZ()       " ,
#     "decay = in_range ( -1.1 * meter, GFAEVX ( GVZ, 100 * meter ), 3 * meter )",
# ]
# tightCut.Cuts      =    {
#     "[Xi-]cc"      : "decay", 
#     "[Lambda0]cc"  : "decay",
#     "[pi-]cc"      : "goodPion" , 
#     "[p+]cc"       : "goodProton",
#     "[mu+]cc"      : "goodMuon"
#                         }
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Vitalii Lisovskyi
# Email: vitalii.lisovskyi@cern.ch
# Date: 20191122
# CPUTime: < 1 min
#
Alias      MyXi     Xi-
Alias      Myanti-Xi anti-Xi+
ChargeConj Myanti-Xi MyXi
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
#
Alias      MyJ/psi       J/psi
ChargeConj MyJ/psi       MyJ/psi
# 
Decay Xi_b-sig 
1.000    MyXi          MyJ/psi      PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyJ/psi
1.000     mu+  mu-         PHOTOS VLL;
Enddecay
#
Decay MyXi
  1.000     MyLambda   pi-      HELAMP   0.551  0.0  0.834  0.0;
Enddecay
CDecay Myanti-Xi
#
Decay MyLambda
  1.000     p+   pi-             HELAMP   0.936   0.0   0.351   0.0;
Enddecay
CDecay Myanti-Lambda
#
End
