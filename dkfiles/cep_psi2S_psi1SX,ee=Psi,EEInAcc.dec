# EventType: 49952002
#
# Descriptor: [psi(2S) -> (J/psi(1S) -> e+ e-) ...]CC
# NickName: cep_psi2S_psi1SX,ee=Psi,EEInAcc
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/cepInAcc
# Production: SuperChic2
#
# InsertPythonCode:
#
# # SuperChic2 options.
# from Configurables import SuperChic2Production
# Generation().Special.addTool(SuperChic2Production)
# Generation().Special.SuperChic2Production.Commands += [
#     "SuperChic2:proc = 52", # Psi(2S)[mu,mu] production.
#     "SuperChic2:decays = false"] # Turn off SuperChic2 decays
#
# # Cuts on the psi(2S).
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool(LoKi__FullGenEventCut, "cepInAcc")
# cepInAcc = Generation().cepInAcc
# cepInAcc.Code = "( count( goodPsi2S ) == 1 )"
# cepInAcc.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import GeV, mrad",
#     "inAcc       = in_range ( 1.95 , GETA , 5.050 ) ",
#     "goodEplus  = GINTREE( ( GID == -11 ) & inAcc )",
#     "goodEminus = GINTREE( ( GID ==  11 ) & inAcc )",
#     "goodPsi2S = ( ( GABSID == 100443 ) & goodEplus & goodEminus )"]
# 
# # Keep the CEP process in MCParticles.
# from Configurables import GenerationToSimulation
# GenerationToSimulation("GenToSim").KeepCode = ("( GBARCODE >= 2 )")
# EndInsertPythonCode
#
# Documentation:
# Central exclusive production of psi(2S)[psi(1S)[e e] X] with electrons in the acceptance
# EndDocumentation
#
# PhysicsWG: EW
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Cristina Sanchez Gras
# Email: cristina.sanchez.gras@cern.ch
# Date: 20201012
#
Alias MyJpsi J/psi
ChargeConj MyJpsi MyJpsi

Decay psi(2S)
0.336000 MyJpsi pi+ pi- VVPIPI;
0.177300 MyJpsi pi0 pi0 VVPIPI;
0.032800 MyJpsi eta PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
0.001300 MyJpsi pi0 PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay

Decay MyJpsi
1.000000 e+ e- PHOTOS VLL;
Enddecay
End
