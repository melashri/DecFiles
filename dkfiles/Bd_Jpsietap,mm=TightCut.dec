# EventType: 11142214
#
# Descriptor: [B0 -> (J/psi(1S) -> mu+ mu-) (eta_prime -> gamma gamma)]cc
#
# NickName: Bd_Jpsietap,mm=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[ B0  => ^( J/psi(1S) => ^mu+ ^mu-)  ( eta_prime -> ^gamma ^gamma ) ]CC'
# tightCut.Cuts      =    {
#     'gamma'     : ' goodGamma ' ,
#     '[mu+]cc'   : ' goodMuon  ' , 
#     'J/psi(1S)' : ' goodPsi   ' }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' , 
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' , 
#     'goodMuon  = ( GPT > 150  * MeV ) & inAcc   ' , 
#     'goodGamma = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY ' ,
#     'goodPsi   = ( GPT > 500  * MeV ) & in_range ( 1.8 , GY , 4.5 )    ' ]
#
# EndInsertPythonCode
#
# Documentation: no CP violation, tight cuts for daughters
# EndDocumentation
#
# PhysicsWG: B2Ch
# Tested: Yes
# Responsible: Max Chefdeville
# Email:       chefdevi@lapp.in2p3.fr
# Date: 20220225
#
Alias       MyJ/psi  J/psi
Alias       MyEtap   eta'
ChargeConj  MyJ/psi  MyJ/psi
ChargeConj  MyEtap   MyEtap
#
Decay B0sig
  1.000         MyJ/psi     MyEtap        SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyJ/psi
  1.000         mu+         mu-          PHOTOS VLL;
Enddecay
#
Decay MyEtap
  1.000         gamma       gamma        PHSP;
Enddecay
End

