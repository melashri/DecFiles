# EventType: 13102611
#
# Descriptor: [B_s0 -> K+ K- pi0 gamma]
#
# NickName: Bs_K+K-pi0gamma=phi1680,TightCut 
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[(Beauty => (X0 => (X+ => ^K+ pi0) ^K-) ^gamma)]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'         : ' inAcc' , 
#     'gamma'          : ' goodPhoton' 
#     }
#
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter',
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     'goodPhoton = ( GPT > 1.8 * GeV ) & InEcal'
#     ]
#
# EndInsertPythonCode
#
# Documentation: Bs-> phi(1680)gamma, phi(1680)->K+K-pi0, gamma PT > 1.8
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20201119
#
Alias      Mypi0      pi0
ChargeConj Mypi0      Mypi0
#
Alias      MyPhi1680      phi(1680)
Alias      Myanti-Phi1680 phi(1680)
ChargeConj MyPhi1680      Myanti-Phi1680
#
Alias      MyK*-          K*-
Alias      MyK*+          K*+
ChargeConj MyK*-          MyK*+
#
Decay B_s0sig
  1.0  MyPhi1680   gamma         PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyPhi1680
  0.5   MyK*-       K+       PHSP;
  0.5   MyK*+       K-       PHSP;
Enddecay
CDecay Myanti-Phi1680
#
Decay MyK*-
  1.0   K-   Mypi0       PHSP;
Enddecay
CDecay MyK*+
#
Decay Mypi0
  1.000        gamma      gamma            PHSP;
Enddecay
#
End
