# EventType: 16275064
# 
# Descriptor: [ Xi_bc+ -> (Lambda_c+ -> p+ K- pi+)  (J/psi -> mu+ mu-) ]cc
#
# NickName: Xibc_LcJpsi,pKpi-res,mm=DecProdCut,m=6.9GeV,t=0.4ps,pKpimumu=TightCut
#
# Production: GenXicc
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# ParticleValue: " Xi_bc+ 532 5242 1.0 6.90000000 0.400000e-12 Xi_bc+ 5242 0.00000000", " Xi_bc~- 533 -5242 -1.0 6.90000000 0.400000e-12 anti-Xi_bc- -5242 0.00000000"
#
# Documentation: decay file of Xi_bc^+ -> (Lambda_c+ -> p K- pi+)  (J/psi -> mu+ mu-)
# using dedicated GenXicc package for production, cuts with XiccDaughtersInLHCb, phase space decay model used. 
# Xi_bc decay time changed to 0.4ps to be in range of theoretical predictions
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutToolWithDecay
# from Gauss.Configuration import *
# generation = Generation()
# production = generation.Special
# production.addTool ( LoKi__GenCutToolWithDecay , 'TightCut' )
# production.CutTool = 'LoKi::GenCutToolWithDecay/TightCut'     
# tightCut   = production.TightCut
# tightCut.SignalPID = production.GenXiccProduction.BaryonState 
# tightCut.Decay     = '[Xi_bc+ ==> (Lambda_c+ ==> ^p+ ^K- ^pi+) (J/psi(1S) => ^mu+ ^mu-)]CC'
# tightCut.Cuts      =    {
#     '[K-]cc'   : ' goodKaon ' ,
#     '[p+]cc'   : ' goodProton ' ,
#     '[pi+]cc'  : ' goodpi  ' ,
#     '[mu-]cc'  : ' goodmu  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import GeV',
#     'inAcc    = in_range ( 0.010 , GTHETA , 0.400 ) ' ,
#     'goodKaon = ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodProton = ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodpi  = ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodmu  = ( GPT > 0.48 * GeV ) & ( GP > 2.98 * GeV ) & inAcc ' ]
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Zhuoming Li, S. Blusk
# Email: zli59@syr.edu, sblusk@syr.edu
# Date: 20180918
#

Alias MyJ/psi J/psi
ChargeConj MyJ/psi MyJ/psi

Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0

Alias      MyDelta++  Delta++
Alias      Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++  Myanti-Delta--

Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-
#
Decay Xi_bc+sig
  1.00   MyLambda_c+  MyJ/psi      PHSP;
Enddecay
CDecay anti-Xi_bc-sig
#
Decay MyLambda_c+
  0.008600000 MyDelta++ K-         PHSP;
  0.010700000 p+      Myanti-K*0   PHSP;
  0.025400000 p+      K-      pi+  PHSP;  #[New mode added] #[Reconstructed PDG2011];
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyDelta++
1.0000    p+  pi+                  PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyK*0
  1.000 K+   pi-                    VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyJ/psi
 1.0000  mu+ mu-             PHOTOS VLL ;
Enddecay

End
