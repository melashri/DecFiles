# EventType: 21103270
#
# Descriptor: [D+ => ( eta => pi+ pi- gamma) K+]cc
#
# NickName: D+_etaK,pipigamma=TightCut 
#
# Cuts: LoKi::GenCutTool/TightCut         
#                                        
# InsertPythonCode:                       
#                                         
# from Configurables import LoKi__GenCutTool 
# gen = Generation()                         
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )  
# tightCut = gen.SignalPlain.TightCut        
# tightCut.Decay     = '^[ D+ => ( eta => ^pi+ ^pi- ^gamma ) ^K+]CC'  
# tightCut.Cuts      =    {                                  
#     '[pi+]cc'    : ' inAcc & dauCuts',                     
#     '[K+]cc'     : ' inAcc & dauCuts',                    
#     '[D+]cc'   : 'Dcuts' }                               
# tightCut.Preambulo += [                                  
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' ,       
#     'dauCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',
#     'Dcuts = (GPT > 1000 * MeV)' ]                  
# EndInsertPythonCode  
#   
#
# Documentation: Forces a D+ to ( eta => pi+ pi- gamma ) K+ with generator level cuts 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Simone Stracka
# Email: simone.stracka@cern.ch
# Date: 20190527
#
Alias       my_eta   eta
ChargeConj  my_eta   my_eta
#
Decay  D+sig
  1.000     my_eta   K+    PHOTOS PHSP ;
Enddecay
CDecay D-sig
#
Decay  my_eta
  1.000     pi+      pi-    gamma  PHOTOS PHSP ;
Enddecay
#
End
