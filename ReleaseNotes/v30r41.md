!========================= DecFiles v30r41 2019-12-16 =======================  
  
! 2019-12-13 - Jacopo Cerasoli (MR !405)  
   Modify 2 decay files (to new event types)
   + 11100014 : Bd_Ksttautau,3pi3pi=DecProdCut,TightCut,tauolababar  
   + 11110000 : Bd_Ksttautau,3pimu=DecProdCut,TightCut,tauolababar   
  
! 2019-12-13 - Serena Maccolini (MR !404)  
   Add 2 new decay files  
   + 21103011 : D+_K-pi+pi+=res,TightCut,ACPKKCuts  
   + 21103101 : D+_Kspi+=phsp,TightCut,ACPKKCuts  
  
! 2019-12-13 - Adrian Casais Vidal (MR !403)  
   Add 7 new decay files  
   + 30000054 : minbias=hardPhoton,pt2GeV-inclusive  
   + 30000056 : minbias=hardPhoton,pt2GeV-tight  
   + 30000055 : minbias=hardPhoton,pt2GeV  
   + 30000053 : minbias=hardPhoton,pt3GeV-inclusive  
   + 30000052 : minbias=hardPhoton,pt3GeV-tight  
   + 30000050 : minbias=hardPhoton,pt3GeV  
   + 30000051 : minbias=hardPhoton,pt4GeV  
  
! 2019-12-13 - Adam Morris (MR !402)  
   Add 4 new decay files  
   + 12495600 : B+_excitedDstDsX,Ds2Xa1=TightCut  
   + 11494600 : Bd_DstDsX,Ds2Xa1=TightCut  
   + 11494601 : Bd_excitedDstDsX,Ds2Xa1=TightCut  
   + 13494600 : Bs_DstDsX,Ds2Xa1=TightCut  
  
! 2019-12-13 - Zishuo Yang (MR !401)  
   Add new decay file  
   + 14543013 : Bc_JpsiMuNu,mm=BcVegPy,ffEbert,TightCut  
  
! 2019-12-12 - Stephen Farry (MR !400)  
   Add 4 new decay files  
   + 42112015 : DrellYan_mumu=20,40GeV  
   + 42112014 : DrellYan_mumu=20GeV  
   + 42102013 : DrellYan_tautau=20GeV,mupt10GeV  
   + 42100004 : Zg_tautau40GeV=mu  
  
! 2019-12-12 - Vsevolod Yeroshenko (MR !399)  
   Add 2 new decay files  
   + 13136000 : Bs_etacphi,KK=DecProdCut,hpt400  
   + 13106012 : Bs_phiphiphi,KK=DecProdCut,hpt400  
  
! 2019-12-11 - Kamil Leszek Fischer (MR !398)  
   Add 4 new decay files  
   + 11876132 : Bd_Dstmunu,KSKK=cocktail,hqet,TightCut,LooserCuts2,BRcorr1  
   + 12875531 : Bu_D0munu,KSKK=cocktail,TightCut2,BRcorr1  
   + 27165902 : Dst_D0pi,KSKK=TightCut,LooserCuts  
   + 27165903 : Dst_D0pi,KSpipi=TightCut,LooserCuts  
  
! 2019-12-11 - Jianqiao Wang (MR !397)  
   Add 4 new decay files  
   + 22162005 : D0_Kpi=DecProdCut,D0PtCut=10GeV  
   + 22162006 : D0_Kpi=DecProdCut,D0PtCut=12GeV  
   + 22162003 : D0_Kpi=DecProdCut,D0PtCut=6GeV  
   + 22162004 : D0_Kpi=DecProdCut,D0PtCut=8GeV  
  
! 2019-12-11 - Giulia Tuci (MR !396)  
   Add new decay file  
   + 27165901 : Dst_D0pi,KSKS=DecProdCut  
  
! 2019-12-11 - Federico Betti (MR !395)  
   Add 6 new decay files  
   + 11874014 : Bd_DstmuX,KK=cocktail,hqet,TightCut,TurboSLCuts,BRCorr1  
   + 11874013 : Bd_DstmuX,Kpi=cocktail,hqet,TightCut,TurboSLCuts,BRCorr1  
   + 11874015 : Bd_DstmuX,pipi=cocktail,hqet,TightCut,TurboSLCuts,BRCorr1  
   + 12873401 : Bu_D0muX,KK=cocktail,TightCut,TurboSLCuts  
   + 12873400 : Bu_D0muX,Kpi=cocktail,TightCut,TurboSLCuts  
   + 12873403 : Bu_D0muX,pipi=cocktail,TightCut,TurboSLCuts  
  
! 2019-12-11 - Marian Stahl (MR !394)  
   Add 2 new decay files  
   + 11576030 : Bd_Dstmunu,Kpipipi=AMPGEN,TightCut  
   + 11166001 : Bd_Dstpi,Kpipipi=AMPGEN,TightCut  
  
! 2019-12-10 - Sophie Elizabeth Hollitt (MR !393)  
   Add 2 new decay files  
   + 11494010 : Bd_D0D0bar,hh,hhhh=AmpGen,MINT,DecProdCut,pCut1600MeV  
   + 13494010 : Bs_D0D0bar,hh,hhhh=AmpGen,MINT,DecProdCut,pCut1600MeV  
  
! 2019-12-08 - Donal Hill (MR !392)  
   Add new decay file  
   + 11196220 : Bd_DsstDst,Dsgamma,D0pi=DDALITZ,DecProdCut  
  
! 2019-12-07 - Michal Kreps (MR !391)  
   Remove character encoding detection in scripts as not all installations of python2 have chardet module.  
  
! 2019-12-06 - Jana Crkovska (MR !390)  
   Add new decay file  
   + 49000226 : gamma=pt5,pt10GeV,Modified  
  
! 2019-12-06 - Niladri Sahoo (MR !389)  
   Add 6 new decay files  
   + 13254200 : Bs_chicphi,JpsigKK,ee=TightCuts  
   + 13244204 : Bs_chicphi,JpsigKK,mm=TightCut  
   + 13156000 : Bs_psi2Sphi,Jpsipipi,ee=CPV,DecProdCut  
   + 12155040 : Bu_JpsiphiK,eeKK=DecProdCut  
   + 15154050 : Lb_Lambda1520psi2S,ee=DecProdCut  
   + 15144070 : Lb_Lambda1520psi2S,mm=DecProdCut  
  
! 2019-12-06 - Stefano Cali (MR !388)  
   Add new decay file  
   + 13594223 : Bs_DsstDsst,DsgammaDsgamma,KKp,Xmunu=cocktail,mu3hInAcc,TightCut  
  
! 2019-12-04 - Ryan Lawrence Newcombe (MR !387)  
   Add 4 new decay files  
   + 12813412 : Bu_Delpbarmunu,pX=TightCutpQCD  
   + 12813402 : Bu_pNstmunu,pX=TightCutpQCD  
   + 12513051 : Bu_ppmunu=DecProdCutpQCD  
   + 12513061 : Bu_pptaunu,mununu=DecProdCutpQCD  
  
! 2019-01-25 - Simone Bifani (MR !205)  
   Script to check use of -> in decay descriptors of LoKi cuts.  

