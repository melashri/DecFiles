DecFiles v30r73 2022-04-30 
==========================  
 
! 2022-04-21 - Arnau Brossa Gonzalo (MR !1072)  
   Add 3 new decay files  
   + 23103440 : Ds+_phipi,pipipi0,gg=DecProdCut  
   + 23203490 : Ds+_pipipipi0,gg=Cocktail,DecProdCut  
   + 23103490 : Ds+_pipipipi0,gg=DecProdCut  
   Modify decay file  
   + 23103471 : Ds+_omegapi,pipipi0,gg=DecProdCut  
  
! 2022-04-20 - Michael Kent Wilkinson (MR !1068)  
   Add 40 new decay files  
   + 30122231 : minbias=eta_gammaA,ee,displaced,mA=100MeV  
   + 30122227 : minbias=eta_gammaA,ee,displaced,mA=10MeV  
   + 30122232 : minbias=eta_gammaA,ee,displaced,mA=150MeV  
   + 30122228 : minbias=eta_gammaA,ee,displaced,mA=15MeV  
   + 30122233 : minbias=eta_gammaA,ee,displaced,mA=200MeV  
   + 30122229 : minbias=eta_gammaA,ee,displaced,mA=20MeV  
   + 30122234 : minbias=eta_gammaA,ee,displaced,mA=250MeV  
   + 30122235 : minbias=eta_gammaA,ee,displaced,mA=300MeV  
   + 30122236 : minbias=eta_gammaA,ee,displaced,mA=350MeV  
   + 30122237 : minbias=eta_gammaA,ee,displaced,mA=400MeV  
   + 30122238 : minbias=eta_gammaA,ee,displaced,mA=450MeV  
   + 30122239 : minbias=eta_gammaA,ee,displaced,mA=500MeV  
   + 30122230 : minbias=eta_gammaA,ee,displaced,mA=50MeV  
   + 30122226 : minbias=eta_gammaA,ee,displaced,mA=5MeV  
   + 30122217 : minbias=eta_gammaA,ee,prompt,mA=100MeV  
   + 30122213 : minbias=eta_gammaA,ee,prompt,mA=10MeV  
   + 30122218 : minbias=eta_gammaA,ee,prompt,mA=150MeV  
   + 30122214 : minbias=eta_gammaA,ee,prompt,mA=15MeV  
   + 30122219 : minbias=eta_gammaA,ee,prompt,mA=200MeV  
   + 30122215 : minbias=eta_gammaA,ee,prompt,mA=20MeV  
   + 30122220 : minbias=eta_gammaA,ee,prompt,mA=250MeV  
   + 30122221 : minbias=eta_gammaA,ee,prompt,mA=300MeV  
   + 30122222 : minbias=eta_gammaA,ee,prompt,mA=350MeV  
   + 30122223 : minbias=eta_gammaA,ee,prompt,mA=400MeV  
   + 30122224 : minbias=eta_gammaA,ee,prompt,mA=450MeV  
   + 30122225 : minbias=eta_gammaA,ee,prompt,mA=500MeV  
   + 30122216 : minbias=eta_gammaA,ee,prompt,mA=50MeV  
   + 30122212 : minbias=eta_gammaA,ee,prompt,mA=5MeV  
   + 30122211 : minbias=pi0_gammaA,ee,displaced,mA=100MeV  
   + 30122207 : minbias=pi0_gammaA,ee,displaced,mA=10MeV  
   + 30122208 : minbias=pi0_gammaA,ee,displaced,mA=15MeV  
   + 30122209 : minbias=pi0_gammaA,ee,displaced,mA=20MeV  
   + 30122210 : minbias=pi0_gammaA,ee,displaced,mA=50MeV  
   + 30122206 : minbias=pi0_gammaA,ee,displaced,mA=5MeV  
   + 30122205 : minbias=pi0_gammaA,ee,prompt,mA=100MeV  
   + 30122201 : minbias=pi0_gammaA,ee,prompt,mA=10MeV  
   + 30122202 : minbias=pi0_gammaA,ee,prompt,mA=15MeV  
   + 30122203 : minbias=pi0_gammaA,ee,prompt,mA=20MeV  
   + 30122204 : minbias=pi0_gammaA,ee,prompt,mA=50MeV  
   + 30122200 : minbias=pi0_gammaA,ee,prompt,mA=5MeV  
  
! 2022-04-18 - Aodhan Burke (MR !1067)  
   Add new decay file  
   + 11774014 : Bd_DstX,cocktail,D0pi,Kpi=DecProdCut  
  
! 2022-04-14 - Bo Fang (MR !1066)  
   Add 9 new decay files  
   + 12997623 : B+_DstXc,Xc2hhhNneutrals_cocktail_3pi,upto5prongs=DecProdCut  
   + 12997624 : B+_DstXc,Xc2hhhNneutrals_cocktail_Kpih,upto5prongs=DecProdCut  
   + 12997625 : B+_DstXc,Xc2hhhNneutrals_cocktail_SL,upto5prongs=DecProdCut  
   + 11896623 : Bd_DstXc,Xc2hhhNneutrals_cocktail_3pi,upto5prongs=DecProdCut  
   + 11896624 : Bd_DstXc,Xc2hhhNneutrals_cocktail_Kpih,upto5prongs=DecProdCut  
   + 11896625 : Bd_DstXc,Xc2hhhNneutrals_cocktail_SL,upto5prongs=DecProdCut  
   + 13996621 : Bs_DstXc,Xc2hhhNneutrals_cocktail_3pi,upto5prongs=DecProdCut  
   + 13996622 : Bs_DstXc,Xc2hhhNneutrals_cocktail_Kpih,upto5prongs=DecProdCut  
   + 13996623 : Bs_DstXc,Xc2hhhNneutrals_cocktail_SL,upto5prongs=DecProdCut  
  
! 2022-04-13 - Qundong Han (MR !1065)  
   Add new decay file  
   + 13106102 : Bs_XipXim,Lambda0pi=DecProdCut  
  
! 2022-01-25 - Ignacio Alberto Monroy Canon (MR !962)  
   Add new decay file  
   + 26164027 : Sigmac3060_D+p+,Kpi=TightCut  
  
