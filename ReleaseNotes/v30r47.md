!========================= DecFiles v30r47 2020-07-07 =======================  
  
! 2020-07-06 - Louis Lenard Gerken (MR !548)  
   Add new decay file  
   + 11196009 : Bd_D+D-,KKpi,Kpipi=CPV,DDALITZ,DecProdCut,pCut1600MeV  
  
! 2020-07-03 - Patrick Haworth Owen (MR !547)  
   Add 3 new decay files  
   + 11874002 : Bd_Dststtaunu,D+,mununu=Cocktail,RDplusCut  
   + 13874003 : Bs_Dsststmunu,D+=cocktail,RDplusCut  
   + 12874001 : Bu_Dststtaunu,D+,mununu=Cocktail,RDplusCut  
  
! 2020-07-03 - Antonio Romero Vidal (MR !546)  
   Add 12 new decay files  
   + 11196018 : Bd_D-Ds+,Kpi,pipipi=DDalitz,TightCut  
   + 11266008 : Bd_D-pipipi,Kpipi=TightCut  
   + 11563002 : Bd_D-taunu,Kpipi,3pinu,tauolababar=TightCut  
   + 11563003 : Bd_D-taunu,Kpipi,3pipi0nu,tauola=TightCut  
   + 11563411 : Bd_Dst-taunu,D-pi0,D-gamma,Kpipi,3pinu,tauolababar=TightCut  
   + 11563421 : Bd_Dst-taunu,D-pi0,D-gamma,Kpipi,3pipi0nu,tauola=TightCut  
   + 12195049 : Bu_D0Ds-,Kpi,pipipi=DDalitz,TightCut  
   + 12265008 : Bu_D0pipipi,Kpi-withf2=TightCut  
   + 12562001 : Bu_D0taunu,Kpi,3pinu,tauolababar=TightCut  
   + 12562011 : Bu_D0taunu,Kpi,3pipi0nu,tauola=TightCut  
   + 12562411 : Bu_Dst0taunu,D0pi0,D0gamma,Kpi,3pinu,tauolababar=TightCut  
   + 12562421 : Bu_Dst0taunu,D0pi0,D0gamma,Kpi,3pipi0nu,tauola=TightCut  
  
! 2020-07-02 - Cristina Sanchez Gras (MR !545)  
   Add 4 new decay files  
   + 49152002 : cep_psi1S_ee=Psi,EEInAcc  
   + 49142002 : cep_psi1S_mumu=Psi,MuMuInAcc  
   + 49152003 : cep_psi2S_ee=Psi,EEInAcc  
   + 49142003 : cep_psi2S_mumu=Psi,MuMuInAcc  
  
! 2020-07-02 - Guy Henri Maurice Wormser (MR !544)  
   Add 4 new decay files  
   + 15298606 : Lb_Lc2593Ds,Ds2hhhNneutrals=DecProdCut,LooseCut  
   + 15298605 : Lb_Lc2625Ds,Ds2hhhNneutrals=DecProdCut,LooseCut  
   + 15298607 : Lb_Sigmac2455Ds,Ds2hhhNneutrals=DecProdCut,LooseCut  
   + 15298608 : Lb_Sigmac2455starDs,Ds2hhhNneutrals=DecProdCut,LooseCut  
  
! 2020-07-02 - Bhagyashree Pagare (MR !543)  
   Add 2 new decay files  
   + 12165095 : Bu_LambdacbarpK,pKpi=sqDalitz,DecProdCut  
   + 12165094 : Bu_Lambdacbarppi,pKpi=sqDalitz,DecProdCut  
  
! 2020-07-02 - Xabier Cid Vidal (MR !542)  
   Add 3 new decay files  
   + 23113421 : Ds_pi+eta,mumu=DecProdCut  
   + 23113200 : Ds_pi+eta,mumug=DecProdCut  
   + 23113430 : Ds_pi+etap,mumu=DecProdCut  
  
! 2020-06-30 - Valeriia Zhovkovska (MR !541)  
   Add 6 new decay files  
   + 10132030 : incl_b=chic0,ppbar,InAcc,PTCut  
   + 10132040 : incl_b=chic1,ppbar,InAcc,PTCut  
   + 10132050 : incl_b=chic2,ppbar,InAcc,PTCut  
   + 28102033 : incl_chic0,pp=Pt0.9GeV  
   + 28102043 : incl_chic1,pp=Pt0.9GeV  
   + 28102053 : incl_chic2,pp=Pt0.9GeV  
  
! 2020-06-28 - Zhihong Shen (MR !540)  
   Add new decay file  
   + 11146114 : Bd_JpsiphiKs,KKmumupipi=DecProdCut  
  
! 2020-06-26 - Vanya Belyaev (MR !539)  
   Add new decay file  
   + 28496041 : X3876_DDstar=TightCut2  
  
! 2020-06-25 - Lauren Emma Yeomans (MR !536)  
   Add new decay file  
   + 11112206 : Bd_gammamumu=ISR,MassCut  
  
! 2020-06-24 - Scott Edward Ely (MR !535)  
   Add 3 new decay files  
   + 15776000 : Lb_Lc2593munu,Lcpipi,pKpi=cocktail,LHCbAcceptance  
   + 15776010 : Lb_Lc2625munu,Lcpipi,pKpi=cocktail,LHCbAcceptance  
   + 15894410 : Lb_LcDs,pKpi,Xmunu,cocktail=LHCbAcceptance  
  
! 2020-06-19 - Donal Hill (MR !533)  
   Add 2 new decay files  
   + 14503060 : Bc_TauNu=BcVegPy,DecProdCut  
   + 14503200 : Bc_TauNuGamma=BcVegPy,DecProdCut  
  
! 2020-06-19 - Serena Maccolini (MR !532)  
   Add 2 new decay files  
   + 23263023 : Ds+_K-K+pi+=res,TightCut,ACPKKCuts  
   + 23103111 : Ds+_KsK+=phsp,TightCut,ACPKKCuts  
   Modify decay files  
   + 21103011 : D+_K-pi+pi+=res,TightCut,ACPKKCuts  
   + 21103101 : D+_Kspi+=phsp,TightCut,ACPKKCuts
  
! 2020-06-19 - Daniel Patrick O'Hanlon (MR !531)  
   Add 6 new decay files  
   + 12575031 : Bu_D0munu,Kpipipi=TightCuts,AmpGen  
   + 12575030 : Bu_D0munu,Kpipipi=TightCuts,PhSp  
   + 12575032 : Bu_D0munu,piKpipi=TightCuts,AmpGen  
   + 27165071 : Dst_D0pi,Kpipipi=TightCuts,AmpGen  
   + 27165070 : Dst_D0pi,Kpipipi=TightCuts,PhSp  
   + 27165072 : Dst_D0pi,piKpipi=TightCuts,AmpGen  
  
! 2020-06-17 - Daniel Charles Craik (MR !529)  
   Add 34 new decay files  
   + 11164483 : Bd_D0Kstar,pipieta,gg=DecProdCut  
   + 11164481 : Bd_D0Kstar,pipipi0,gg=DecProdCut  
   + 11134460 : Bd_JpsiKstar,pipieta,gg=DecProdCut  
   + 11134450 : Bd_JpsiKstar,pipipi0,gg=DecProdCut  
   + 11104493 : Bd_KstarDarkBoson2pi0pipi,gg,m=1000MeV,t=100ps,DecProdCut  
   + 11104483 : Bd_KstarDarkBoson2pi0pipi,gg,m=1000MeV,t=1ps,DecProdCut  
   + 11104494 : Bd_KstarDarkBoson2pi0pipi,gg,m=1500MeV,t=100ps,DecProdCut  
   + 11104484 : Bd_KstarDarkBoson2pi0pipi,gg,m=1500MeV,t=1ps,DecProdCut  
   + 11104495 : Bd_KstarDarkBoson2pi0pipi,gg,m=2000MeV,t=100ps,DecProdCut  
   + 11104485 : Bd_KstarDarkBoson2pi0pipi,gg,m=2000MeV,t=1ps,DecProdCut  
   + 11104496 : Bd_KstarDarkBoson2pi0pipi,gg,m=3000MeV,t=100ps,DecProdCut  
   + 11104486 : Bd_KstarDarkBoson2pi0pipi,gg,m=3000MeV,t=1ps,DecProdCut  
   + 11104497 : Bd_KstarDarkBoson2pi0pipi,gg,m=4000MeV,t=100ps,DecProdCut  
   + 11104487 : Bd_KstarDarkBoson2pi0pipi,gg,m=4000MeV,t=1ps,DecProdCut  
   + 11104491 : Bd_KstarDarkBoson2pi0pipi,gg,m=500MeV,t=100ps,DecProdCut  
   + 11104481 : Bd_KstarDarkBoson2pi0pipi,gg,m=500MeV,t=1ps,DecProdCut  
   + 11104492 : Bd_KstarDarkBoson2pi0pipi,gg,m=750MeV,t=100ps,DecProdCut  
   + 11104482 : Bd_KstarDarkBoson2pi0pipi,gg,m=750MeV,t=1ps,DecProdCut  
   + 11104480 : Bd_Kstaromega,pi0pipi,gg=DecProdCut  
   + 12103495 : Bu_KDarkBoson2pi0pipi,gg,m=1000MeV,t=100ps,DecProdCut  
   + 12103483 : Bu_KDarkBoson2pi0pipi,gg,m=1000MeV,t=1ps,DecProdCut  
   + 12103496 : Bu_KDarkBoson2pi0pipi,gg,m=1500MeV,t=100ps,DecProdCut  
   + 12103484 : Bu_KDarkBoson2pi0pipi,gg,m=1500MeV,t=1ps,DecProdCut  
   + 12103497 : Bu_KDarkBoson2pi0pipi,gg,m=2000MeV,t=100ps,DecProdCut  
   + 12103485 : Bu_KDarkBoson2pi0pipi,gg,m=2000MeV,t=1ps,DecProdCut  
   + 12103498 : Bu_KDarkBoson2pi0pipi,gg,m=3000MeV,t=100ps,DecProdCut  
   + 12103486 : Bu_KDarkBoson2pi0pipi,gg,m=3000MeV,t=1ps,DecProdCut  
   + 12103499 : Bu_KDarkBoson2pi0pipi,gg,m=4000MeV,t=100ps,DecProdCut  
   + 12103487 : Bu_KDarkBoson2pi0pipi,gg,m=4000MeV,t=1ps,DecProdCut  
   + 12103493 : Bu_KDarkBoson2pi0pipi,gg,m=500MeV,t=100ps,DecProdCut  
   + 12103481 : Bu_KDarkBoson2pi0pipi,gg,m=500MeV,t=1ps,DecProdCut  
   + 12103494 : Bu_KDarkBoson2pi0pipi,gg,m=750MeV,t=100ps,DecProdCut  
   + 12103482 : Bu_KDarkBoson2pi0pipi,gg,m=750MeV,t=1ps,DecProdCut  
   + 12103489 : Bu_Komega,pi0pipi,gg=DecProdCut  
  
! 2020-06-15 - Jacopo Cerasoli (MR !528)  
   Add 2 new decay files  
   + 11538000 : Bd_psi2SKst,tautau,3pi3pi=DecProdCut,TightCut,tauolababar  
   + 11746000 : Bd_psi2SKst,tautau,3pimu=DecProdCut,TightCut,tauolababar  
  
! 2020-06-12 - Andrea Merli (MR !527)  
   Add new decay file  
   + 26104184 : Xic_LambdaKpi,ppi=TightCut  
  
! 2020-06-11 - Mengzhen Wang (MR !526)  
   Add 4 new decay files  
   + 16266040 : Xibstar6327_LbKpi,Lb=Lcpi,TightCut  
   + 16268040 : Xibstar6327_LbKpi,Lb=Lcpipipi,TightCut  
   + 16266042 : Xibstar6330_LbKpi,Lb=Lcpi,TightCut  
   + 16268041 : Xibstar6330_LbKpi,Lb=Lcpipipi,TightCut  
  
! 2020-06-11 - Erica Polycarpo Macedo (MR !525)  
   Add 4 new decay files  
   + 21103005 : D+_K-K+pi+=res,TightCut3  
   + 21103023 : D+_pi-pi+pi+=res,TightCut3  
   + 23103024 : Ds+_K-K+pi+=res,TightCut3  
   + 23103013 : Ds+_pi-pi+pi+=res,TightCut3  
  
! 2020-06-10 - Vitalii Lisovskyi (MR !524)  
   Add new decay file  
   + 12583022 : Bu_D0enu,Kenu=TightCut  
   Declare obsolete  
   + 12583021 : Bu_D0enu,Kenu=DecProdCut,TightCut_buggy  
  
! 2020-06-09 - Qiaohong Li (MR !522)  
   Add new decay file  
   + 11144452 : Bd_Jpsiomega1420,mm,pipipi=DecProdCut  
  
! 2020-06-09 - Michal Kreps (MR !521)  
   Use Pythia8 instead of Pythia6 as default production tool  
  
! 2020-06-08 - Da Yu Tou (MR !520)  
   Add new decay file  
   + 11156000 : Bd_psi2SKst,Jpsipipi,ee=DecProdCut  
  
! 2020-06-05 - Michal Kreps (MR !519)  
   - Update list of decay models in decparser.  
   - Add helper script to grab list of models from gitlab Gauss repository.  
  
