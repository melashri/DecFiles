
!========================= DecFiles v30r28 2019-02-14 =======================

! 2019-02-05 - Sophie Baker (MR !209)  
   Add 1 new decay files  
   + 30000242 : deuteron_in_acc.dec  

! 2019-02-12 - Jolanta Brodzicka (MR !221)
   Added 10 new decfiles for Lc->pll rare/forbidden and resonance/normalisation decays
   + 25113031 : Lc_pmumu,phsp=OS,TightCut.dec
   + 25113032 : Lc_pmumu,phsp=OS,TightCut,FromB.dec
   + 25113011 : Lc_pphi,mumu=TightCut.dec
   + 25113021 : Lc_pomega,mumu=TightCut.dec
   + 25113041 : Lc_prho,mumu=TightCut.dec
   + 25123010 : Lc_pee=OS,TightCut.dec
   + 25123011 : Lc_pphi,ee=TightCut.dec
   + 25123012 : Lc_pomega,ee=TightCut.dec
   + 25123013 : Lc_prho,ee=TightCut.dec
   + 25313011 : Lc_pemu=OS,TightCut.dec

! 2019-02-11 - Xabier Cid Vidal (MR !220)
   Add several dec files for Eta prime decays
	 + 12115070: Bu_K+etap,pipimumu=DecProdCut
	 + 23175000: Ds_pi+etap,pipimumu=DecProdCut
   + 23175001: Ds_3piphi,mumu=DecProdCut

! 2019-02-10 - Svende Braun (MR !217)
   Added 4 new decfile for Bs->Kmunu analysis
   + 11442022: Bd_CharmoniumKX,mumu,PPTcuts=TightCut.dec
   + 12442022: Bu_CharmoniumKX,mumu,PPTcuts=TightCut.dec
   + 13444022: Bs_CharmoniumKX,mumu,PPTcuts=TightCut.dec
   + 15143002: Lb_JpsiKp,mumu,PPTcuts=TightCut.dec

! 2019-01-31 - Aravindhan Venkateswaran (MR !208)
	added 4 decfiles 
	 + 16144700 : Lb_JpsiLambdast1405,mm,Sigmapi,Lambdagamma=phsp,DecProdCut.dec
	 + 16144701 : Lb_JpsiLambdast1520,mm,Sigmapi,Lambdagamma=phsp,DecProdCut.dec
	 + 16144702 : Lb_JpsiLambdast1600,mm,Sigmapi,Lambdagamma=phsp,DecProdCut.dec
	 + 16145311 : Lb_chic1Lambda,Jpsig,mm=PHSP,DecProdCut.dec

! 2019-01-23 - David Gerick (MR !204)
   modify two decfiles for B+->K*+ mu mu decays, changing arrows in tightCut.Decay (-> to =>). EventTypes changed incrementally!
   + 12113445 : Bu_Kstmumu,Kpi0=PHSP,flatq2,DecProdCut,TightCut.dec
   + 12115178 : Bu_Kstmumu,KSpi=PHSP,flatq2,DecProdCut,TightCut.dec
   + 12113446 : Bu_Kstmumu,Kpi0=PHSP,flatq2,DecProdCut,TightCut.dec
   + 12115179 : Bu_Kstmumu,KSpi=PHSP,flatq2,DecProdCut,TightCut.dec

! 2018-11-12 - Svende Braun (MR !155)
   Added one new decfile
   + 10040042: incl_b=CharmoniumKX,mumu,PPTcuts.dec

! 2019-02-07 - Marcin Chrzaszcz (MR !216)
   Fix decay files:
   + 21513012 : D+_taunu,mmm=FromB.dec
   + 21513013 : D+_taunu,mmm=FromD.dec
   + 23513015 : Ds_taunu,mmm=FromB.dec
   + 23513016 : Ds_taunu,mmm=FromD.dec
   + 31113003 : tau_mumumu=FromB.dec

! 2019-01-02 - Biplab Dey (MR !190)
   new dec files for Kspipig TD-CPV analysis:
   + 11104351 : Bd_KsK+K-gamma=TightCut,mKshhCut,PHSP.dec
   + 11204301 : Bd_Kspi+pi-gamma=TightCut,mKshhCut,K1cocktail.dec
   + 11104711 : Bd_Kspi+pi-pi0gamma=TightCut,mKshhCut,PHSP.dec
   + 11104562 : Bd_Kspi+pi-pi0=TightCut,mKshhCut,PHSP.dec
   + 13104323 : Bs_KsK+pi-gamma=TightCut,mKshhCut,PHSP.dec
   + 13104512 : Bs_KsK+pi-pi0=TightCut,mKshhCut,PHSP.dec
   + 12105311 : Bu_Kspi+pi-pi+gamma=TightCut,mKshhCut,PHSP.dec
   + 15104372 : Lb_Ksppi-gamma=TightCut,mKshhCut,PHSP.dec
   + 15104571 : Lb_Ksppi-pi0=TightCut,mKshhCut,PHSP.dec

! 2019-01-15 - Stefano Petrucci (MR !199)
   + 11144413 : Bd_Jpsietap,mm,etapipi=TightCut.dec
   + 13144413 : Bs_Jpsietap,mm,etapipi=TightCut.dec
   + 11144203 : Bd_Jpsietap,mm,rhog,pipi=TightCut.dec

! 2019-01-21 - Joao Coelho (MR !145)
   Add 1 decfile for the search for B -> K tau mu
   + 14575600 : Bc_KBs,Dsmunu,3h=cocktail,mu4hinAcc.dec

! 2019-01-21 - Joao Coelho (MR !196)
   Modified 7 decfiles for the search for B -> K tau mu
   + 11466410 : Bd_D0Kpi,4h=cocktail,5hinAcc.dec
   + 11876400 : Bd_D0pimunu,4h=cocktail,mu4hinAcc.dec
   + 11876070 : Bd_D0pitaunu,tau_mu,D0_4h=cocktail,mu4hinAcc.dec
   + 13576010 : Bs_Dststmunu,KD0,4h=cocktail,mu4hinAcc.dec
   + 13896610 : Bs_KDsD0,munu,4h=cocktail,mu4hinAcc.dec
   + 13466000 : Bs_D0Kpi,4h=cocktail,5hinAcc.dec
   + 15876430 : Lb_Lcpipimunu,2hX=cocktail,mu4hinAcc.dec

! 2019-01-21 - Martha Hilton (MR !201)
   Add two new Dec Files for D02KSPiPi analysis
   + 11875120 : Bd_D0munu,KSpipi=cocktail,hqet,TightCut.dec
   + 12875523 : Bu_D0munu,KSpipi=cocktail,Loose,TightCut.dec

! 2019-01-22 - Marco Pappagallo
   + 26164085 : Xic27900_LcK,pKpi=phsp,TightCut.dec
   + 26164086 : Xicstst0_LcK,pKpi=phsp,TightCut,m=2920MeV,G=10MeV.dec
   + 26164087 : Xicstst0_LcK,pKpi=phsp,TightCut,m=2940MeV,G=10MeV.dec
   + 26164088 : Xic29700_LcK,pKpi=phsp,TightCut.dec
   + 26164089 : Xic30800_LcK,pKpi=phsp,TightCut.dec

! 2019-01-30 - David Gerick (MR !206)
   + Improve descriptor check in decparser
   + Improve documentation on decay descriptors in LoKi cuts

! 2019-02-07 - Vincent TISSERAND (MR !215)
   + 13164664 : Bs_Dst0phi,D0gamma,Kpipi0=DecProdCut,HELAMP100.dec
   + 13164665 : Bs_Dst0phi,D0gamma,Kpipi0=DecProdCut,HELAMP010.dec
   + 13164666 : Bs_Dst0phi,D0gamma,Kpipi0=DecProdCut,HELAMP001.dec
   + 13164467 : Bs_Dst0phi,D0pi0,Kpipi0=DecProdCut,HELAMP100.dec
   + 13164468 : Bs_Dst0phi,D0pi0,Kpipi0=DecProdCut,HELAMP010.dec
   + 13164469 : Bs_Dst0phi,D0pi0,Kpipi0=DecProdCut,HELAMP001.dec

! 2019-01-14 - Jolanta Brodzicka (MR !197)
   add new decay file with correct arrows for prompt D0->KsPiPi 
   + 27165179 : Dst_D0pi,KSpipi=TightCut,For2012,DoubleArrows.dec

! 2018-12-21 - Maximilien Chefdefille (MR !188)
   + 13144213 : Bs_Jpsieta,mm,pipig=TightCut.dec
   + 13142212 : Bs_Jpsietap,mmgg=TightCut.dec

! 2019-01-05 - Mengzhen Wang (MR !191, MR 200)
   add two decay files for Lb->Sigmac D K decays
   + 15298005 : Lb_Sigmac++D-K,Lcpi_pKpi,Kpipi=DecProdCut.dec
   + 15298006 : Lb_Sigmac++Dst-K,Lcpi_pKpi,D0pi_Kpi=DecProdCut.dec

! 2019-01-09 - Tom Hadavizadeh (MR !192) 
   Added 16 decay files 
      + 21123203 : D+_pi+pi0,eeg=TightCut3.dec 
      + 21123231 : D+_K+pi0,eeg=TightCut2.dec  
      + 21123240 : D+_pi+eta,eeg=TightCut3 
      + 21123211 : D+_K+eta,eeg=TightCut2.dec  
      + 23123210 : Ds_pi+pi0,eeg=TightCut2.dec 
      + 23123203 : Ds_K+pi0,eeg=TightCut2.dec  
      + 23123231 : Ds_pi+eta,eeg=TightCut2.dec 
      + 23123230 : Ds_K+eta,eeg=TightCut2.dec      
      + 21101402 : D+_pi+pi0,gg=TightCut2.dec  
      + 21101421 : D+_K+pi0,gg=TightCut2.dec 
      + 21101411 : D+_pi+eta,gg=TightCut2.dec  
      + 21101431 : D+_K+eta,gg=TightCut2.dec 
      + 23101402 : Ds_pi+pi0,gg=TightCut2.dec  
      + 23101421 : Ds_K+pi0,gg=TightCut2.dec 
      + 23101411 : Ds_pi+eta,gg=TightCut2.dec  
      + 23101431 : Ds_K+eta,gg=TightCut2.dec  
   This new set of decay files changes the generator level cut descriptor arrows from '->' to '=>' and modified the associated event types. 


! 2019-01-15 - Mengzhen Wang (MR !198)
   add a new decfile for the Xicc++ -> Lambda_c+ pi+ decay with updated Xicc++ lifetime
   + 26264055 : Xicc++_Lcpi,pKpi-res=GenXicc,DecProdCut,t=256fs,WithMinPT,3GeV.dec

! 2019-01-10 - Qungnian Xu (MR !193)
   + 12145062 : Bu_X4160,Jpsiphi=DecProdCut,InAcc.dec
   + 12145061 : Bu_X4350,Jpsiphi=DecProdCut,InAcc.dec
   + 15174020 : Lb_Pc4312++pi,Jpsip=DecProdCut.dec
   + 15174021 : Lb_Pc4440++pi,Jpsip=DecProdCut.dec
   + 15174022 : Lb_Pc4457++pi,Jpsip=DecProdCut.dec
   + 15174023 : Lb_Zc3900++pi,Jpsip=DecProdCut.dec
   + 15174024 : Lb_Zc4200++pi,Jpsip=DecProdCut.dec
   + 15174025 : Lb_Zc4430++pi,Jpsip=DecProdCut.dec
   + 26143011 : Pc4312,Jpsip=DecProdCut,InAcc.dec
   + 26143012 : Pc4440,Jpsip=DecProdCut,InAcc.dec
   + 26143013 : Pc4457,Jpsip=DecProdCut,InAc.dec
   + 28144061 : X4160,Jpsiphi=DecProdCut,InAcc.dec
   + 28144060 : X4350,Jpsiphi=DecProdCut,InAcc.dec
   + 26143014 : Zc3900,Jpsipi=DecProdCut,InAcc.dec
   + 26143015 : Zc4200,Jpsipi=DecProdCut,InAcc.dec
   + 26143016 : Zc4430,Jpsipi=DecProdCut,InAcc.dec

! 2019-01-11 - Ao Xi (MR !195)
   add new decfile
   + 26104880 : Xic0_pKKpi=phsp,DecProdCut,TightCut,tau=250fs.dec

! 2018-12-21 - Fabrice Desse (MR !187)
   add new decfile
   + 11124009 : Bd_Kstee,flatq2=DecProdCut,MomCut,TightCut600MeV.dec 

! 2018-12-21 - Jascha Grabowski (MR !189)
   add new decfiles with generator level cuts for B2psi2Spipi analysis
   + 11146092 : Bd_psi2Spipipipi,mm=phsp,DecProdCut,TightCut.dec
   + 13146055 : Bs_psi2Spipipipi,mm=phsp,DecProdCut,TightCut.dec
   + 13144044 : Bs_psi2SKK,mm=phsp,DecProdCut,TightCut.dec

