DecFiles v30r58 2021-04-21 
==========================  
 
! 2021-04-21 - Michal Kreps (MR !725)  
   - Add Sigma_c* as charm signal particle.
   - Add LbAmpGen to the list of decay models.
   - In decparser separate tests, where failure means we cannot accept and those which need further checking (make these as warning) and fail pipeline if something fails.  
  
! 2021-04-19 - Federica Oliva (MR !724)  
   Add 4 new decay files  
   + 26265970 : Xicstst+2930_LcKpi,pKpi=phsp,TightCut  
   + 26265971 : Xicstst+2970_LcKpi,pKpi=phsp,TightCut  
   + 26265972 : Xicstst+3055_LcKpi,pKpi=phsp,TightCut  
   + 26265973 : Xicstst+3080_LcKpi,pKpi=phsp,TightCut  
  
! 2021-04-16 - Lorenzo Paolucci (MR !723)  
   Add new decay file  
   + 59963001 : gun_Dst_D0pi,D0_Kpi=NoCut  
  
! 2021-04-16 - Valeriia Zhovkovska (MR !722)  
   Add 3 new decay files  
   + 28104030 : incl_chic0,phiphi,KK=TightCut  
   + 28104040 : incl_chic1,phiphi,KK=TightCut  
   + 28104050 : incl_chic2,phiphi,KK=TightCut  
   Modify 3 decay files  
   + 10134030 : incl_b=chic0,phiphi,KK,InAcc  
   + 10134040 : incl_b=chic1,phiphi,KK,InAcc  
   + 10134050 : incl_b=chic2,phiphi,KK,InAcc  
  
! 2021-04-14 - Francesco Polci (MR !721)  
   Add new decay file  
   + 11123000 : Bd_Ksttaue,3pi=DecProdCut,tauolababar  
  
! 2021-04-14 - Lukas Calefice (MR !720)  
   Add 3 new decay files  
   + 12155111 : Bu_JpsipLambda,ee=DecProdCut  
   + 12125190 : Bu_Lambdapbaree=DecProdCut  
   + 12115190 : Bu_Lambdapbarmumu=DecProdCut  
  
! 2021-04-13 - Ziyi Wang (MR !719)  
   Add 7 new decay files  
   + 26104188 : Omegac0_L0KS0,ppi,pipi=pshp,DecProdCut  
   + 26104191 : Omegac0_L0Kpi,ppi=pshp,DecProdCut  
   + 26102081 : Omegac0_pK-=pshp,DecProdCut  
   + 26103180 : Xic+_pKS0,pipi=pshp,DecProdCut  
   + 26104189 : Xic0_L0KS0,ppi,pipi=pshp,DecProdCut  
   + 26104190 : Xic0_L0Kpi,ppi=pshp,DecProdCut  
   + 26102080 : Xic0_pK-=pshp,DecProdCut  
  
! 2021-04-12 - Michel De Cian (MR !717)  
   Add 3 new decay files  
   + 11874402 : Bd_D+munu,pipipi=cocktail,Dalitz,TightCut,ForB2RhoMuNu  
   + 12875408 : Bu_D0munu,Kpipipi=cocktail,BRcorr1,TightCut,ForB2RhoMuNu  
   + 12873425 : Bu_D0munu,pipipi0pi0=cocktail,BRcorr1,TightCut,ForB2RhoMuNu  
  
! 2021-04-09 - Marian Stahl (MR !716)  
   Add 2 new decay files  
   + 15344400 : Lb_JpsiSigma+pi=TightCut  
   + 15144800 : Lb_JpsiSigma-pi=TightCut  
  
! 2021-04-08 - Marian Stahl (MR !715)  
   Add new decay file  
   + 26105197 : Xic_Xipipi=Downstream,AMPGEN,TightCut  
  
! 2021-03-30 - Liang Sun (MR !713)  
   Add 2 new decay files  
   + 27583470 : Dst_D0pi,rhoenu=DecProdCut  
   + 27573470 : Dst_D0pi,rhomunu=DecProdCut  
  
! 2021-03-29 - Andrii Usachov (MR !712)  
   Add 2 new decay files  
   + 14543026 : Bc_Jpsimunu,pp=BcVegPy,ffEbert,DecProdCut  
   + 14543025 : Bc_Jpsimunu,pp=BcVegPy,ffKiselev,DecProdCut  
  
! 2021-03-26 - Aodhan Burke (MR !710)  
   Add new decay file  
   + 27165073 : Dst_D0pi,Kpipipi=DecProdCut,AmpGen  
  
! 2021-03-26 - Qiuchan Lu (MR !709)  
   Add 2 new decay files  
   + 30000002 : minbias=BiasedPbarPt300MeV  
   + 30000003 : minbias=BiasedXiLambdabarPbarPt300MeV  
  
! 2021-03-26 - Qiuchan Lu (MR !708)  
   Add new decay file  
   + 11166004 : Bd_Lcpipip,pKpi=TightCut  
  
! 2021-03-24 - Hanae Tilquin (MR !707)  
   Add 2 new decay files  
   + 13874242 : Bs_Dsmunu,phimunuCocktail=TightCut  
   + 13614041 : Bs_phitautau,mumuCocktail=TightCut  
  
! 2021-03-22 - Dan Thompson (MR !706)  
   Modify 2 decay files  
   + 15314000 : Lb_Lambda1520emu,pK=DecProdCut  
   + 15314030 : Lb_emupK=DecProdCut  
  
! 2021-03-19 - Christina Agapopoulou (MR !705)  
   Add new decay file  
   + 11104000 : Bd_pi+pi-Kst=sqDalitz,DecProdCut  
  
! 2021-03-18 - Ryunosuke O'Neil (MR !703)  
   Add 4 new decay files  
   + 26266030 : Xic28150_Xicpipi,pKKpi=phsp,TightCut  
   + 26266031 : Xic29230_Xicpipi,pKKpi=phsp,TightCut  
   + 26266032 : Xic29700_Xicpipi,pKKpi=phsp,TightCut  
   + 26266033 : Xic30550_Xicpipi,pKKpi=phsp,TightCut  
  
! 2021-03-18 - Julien Cogan (MR !702)  
   Add 2 new decay files  
   + 11716402 : Bd_Ksttautau,3pipi0mu=DecProdCut,TightCut,tauolababar  
   + 11716400 : Bd_Ksttautau,3pipi0mu=DecProdCut,TightCut  
  
! 2021-03-17 - Harry Victor Cliff (MR !700)  
   Add 2 new decay files  
   + 11114036 : Bd_aa2mumumumu,m=1GeV,t=1fs,DecProdCut  
   + 13114035 : Bs_aa2mumumumu,m=1GeV,t=1fs,DecProdCut  
  
! 2021-03-16 - Liang Sun (MR !699)  
   Add 2 new decay files  
   + 21523020 : D+_K-pi+e+nu=BESIIImodel,DecProdCut  
   + 21513020 : D+_K-pi+mu+nu=BESIIImodel,DecProdCut  
  
! 2021-03-16 - Ricardo Vazquez Gomez (MR !697)  
   Add 7 new decay files  
   + 11666000 : Bd_Dststtaunu,D-=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
   + 11563400 : Bd_Dststtaunu,D0Dst=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
   + 13666000 : Bs_Dsststtaunu,D-=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
   + 13863000 : Bs_Dsststtaunu,D0Dst=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
   + 12666000 : Bu_Dststtaunu,D-=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
   + 12863030 : Bu_Dststtaunu,D0Dst=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
   + 15663000 : Lb_Lcsttauunu,D0p=cocktail,3pinu3pipi0nu,tauolababar,TightCut  
  
! 2021-03-12 - Andrii Usachov (MR !694)  
   Add new decay file  
   + 18100200 : incl_etab,gammagamma=UpsilonDaughtersInLHCb  
  
! 2021-03-02 - John Smeaton (MR !690)  
   Add 4 new decay files  
   + 11584100 : Bd_Denu,KSenu=TightCut,EvtGenDecayWithCut  
   + 11584101 : Bd_Denu,KSpi=TightCut  
   + 11584110 : Bd_Dpi,KSenu=TightCut  
   + 12105182 : Bu_Kstpipi,KSpi=TightCut  
  
! 2021-02-21 - Jacco Andreas De Vries (MR !684)  
   Add new decay file  
   + 26103092 : Xic_pKpi=phsp,TightCutv3  
  
