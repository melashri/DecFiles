  
!========================= DecFiles v30r30 2019-05-22 =======================  
  
! 2019-05-22 - Andre Guenther (MR !290)  
   Update arrow in TightCut tool to include FSR  
   + 27184010 : Dst0_D0ee,Kpi=TightCut.dec  
   + 27162212 : Dst0_D0gamma,Kpi=TightCut,gammaConv.dec  
   + 27184410 : Dst0_D0pi0,Kpi=TightCut,e+e-g=Dalitz.dec  
   + 27162413 : Dst0_D0pi0,Kpi=TightCut,gammaConv.dec  
  
! 2019-05-21 - Hannah Pullen (MR !288)  
   Added 11 decfiles:  
   + 11166106 : Bd_D0Kst,KSpipi=B-SVS,D-PHSP,TightCut,LooserCuts.dec  
   + 11166116 : Bd_D0Kst,KSKK=B-SVS,D-PHSP,TightCut,LooserCuts.dec  
   + 11166123 : Bd_D0rho0,KSpipi=TightCut,LooserCuts.dec  
   + 13166104 : Bs_D0Kst,KSpipi=B-SVS,D-PHSP,TightCut,LooserCuts.dec  
   + 13166124 : Bs_D0Kst,KSKK=B-SVS,D-PHSP,TightCut,LooserCuts.dec  
   + 13166305 : Bs_Dst0Kst0,D0gamma,KSpipi=TightCut,LooserCuts,HELAMP100.dec  
   + 13166306 : Bs_Dst0Kst0,D0gamma,KSpipi=TightCut,LooserCuts,HELAMP010.dec  
   + 13166307 : Bs_Dst0Kst0,D0gamma,KSpipi=TightCut,LooserCuts,HELAMP001.dec  
   + 13166505 : Bs_Dst0Kst0,D0pi0,KSpipi=TightCut,LooserCuts,HELAMP100.dec  
   + 13166506 : Bs_Dst0Kst0,D0pi0,KSpipi=TightCut,LooserCuts,HELAMP010.dec  
   + 13166507 : Bs_Dst0Kst0,D0pi0,KSpipi=TightCut,LooserCuts,HELAMP001.dec  
  
! 2019-05-21 - Roberta Cardinale (MR !286)  
   Fix => decay issue in:  
   + Bd_ppKK=DecProdCut,TightCut.dec (changed evID from 11104073 to 11104078)  
   + Bd_pppipi=DecProdCut,TightCut.dec (changed evID from 11104074 to 11104079)  
   + Bs_ppKK=DecProdCut,TightCut.dec (changed evID from 13104062 to 13104060)  
   + Bs_ppKpi=DecProdCut,TightCut.dec (changed evID from 13104063 to 13104068)  
   + Bs_pppipi=DecProdCut,TightCut.dec (changed evID from 13104064 to 13104069)  
  
! 2019-05-18 - Vanya Belyaev (MR !285)  
   Add new decay files  
   + 16146015 : bstar6080_Lbpipi,JpsipK=TightCut.dec  
   + 16166015 : Lbstar6080_Lbpipi,Lcpi=TightCut.dec  
   + 16146014 : Lbstar6150_Lbpipi,JpsipK=TightCut.dec  
   Fix decay file  
   + 16166014 : Lbstar6150_Lbpipi,Lcpi=TightCut.dec  
  
! 2019-05-17 - Maciej Wojciech Dudek (MR !284)  
   Add new decay file  
   + 12105021 : Bu_phiphiK=TightCut.dec  
  
! 2019-03-22 - Anna Danilina (MR !257)  
   Add new decay files  
   + 12523000 : Bu2EEENuE=DN,DecProdCut.dec  
   + 12513021 : Bu2MuMuMuNuM=DN,DecProdCut.dec  
   + 12513023 : Bu2EEMuNuM=DN,DecProdCut.dec  
   + 12513022 : Bu2MuMuENuE=DN,DecProdCut.dec  
  
! 2019-05-02 - Ricardo Vazquez Gomez (MR !277)  
   Add new event types  
   + 27163684 : Ds2460_Ds2317gamma,Dspi0,KKpi=DecProdCut.dec  
   + 27163685 : Ds2460_Ds2317gamma,Dspi0,KKpi=TightCut.dec  
   + 27163421 : Dsst_Dspi0,KKpi=DecProdCut.dec  
   Declare event type 27163682 obsolete.  
  
! 2019-05-09 - Daniel O'Hanlon (MR !281)  
   Add four new decfiles:  
   + 15164521 : Lb_Sigmac+pi-,Lambdac+pi0=DecProdCut.dec  
   + 15104582 : Lb_pKSKst-=DecProdCut.dec  
   + 15104581 : Lb_pKSrho-=DecProdCut.dec  
   + 15104590 : Lb_ppiKst0,KSpi0=DecProdCut.dec  
  
! 2019-05-10 - Marie Bachmayer (MR !282)  
   Add 2 decfiles  
   + 11576010 : Bd_Dmunu,Kst0a1,Kpipipipi=DecProdCut,TightCut.dec  
   + 11576020 : Bd_Dpimunu,a1,Kpipipi=DecProdCut,TightCut.dec  
  
! 2019-05-07 - Veronika Chobanova (MR !280)  
   Added decay file  
   + 13144011 Bs_Jpsiphi,mm=CPV,update2016,dG=0,DecProdCut.dec  
  
! 2019-03-12 - Lorenzo Sestini (MR !251)  
   Add 15 new decay files  
   + 40900003 : gg_Higgs_cc=mH125GeV,2cinAcc.dec  
   + 40900010 : A1_bb=25GeV.dec  
   + 40900011 : A1_bb=35GeV.dec  
   + 40900012 : A1_bb=45GeV.dec  
   + 40900013 : A1_bb=60GeV.dec  
   + 40900014 : A1_bb=80GeV.dec  
   + 40900015 : A1_bb=100GeV.dec  
   + 40900016 : A1_bb=175GeV.dec  
   + 40900020 : A1_cc=25GeV.dec  
   + 40900021 : A1_cc=35GeV.dec  
   + 40900022 : A1_cc=45GeV.dec  
   + 40900023 : A1_cc=60GeV.dec  
   + 40900024 : A1_cc=80GeV.dec  
   + 40900025 : A1_cc=100GeV.dec  
   + 40900026 : A1_cc=175GeV.dec  
  
! 2019-05-02 - Michael Wilkinson (MR !279)  
   updated one (so far unused) DecFile to use upcoming LHCb Xi_c0 lifetime measurement  
   + 26264081 : Xic0_Lcpi,pKpi-res=LHCbAcceptance.dec  
  
! 2019-03-15 - Yu Lu (MR !252)  
   Add new decay file  
   + 12197003: Bu_LcLcK,pKpi,pKPi=PHSP,DecProdCut.dec  
  
! 2019-05-02 - Laurent Dufour (MR !278)  
   Update production tool in DiMuonBothSignP3GeVMassCut.py option file.  
  
! 2019-05-14 - Michal Kreps (MR !283)  
   Fix CMakeLists.txt which dependend on DecFilesTests project not used anymore. This fixes build of the package within cmake Gauss build.  
   
! 2019-04-15 - Cristina Sanchez Gras (MR !269)  
   Added two new decay files for CEP of psi(1S) mesons.  
   + 49142000 : cep_psi1S_mumu.dec  
   + 49152000 : cep_psi1S_ee.dec  
  
! 2019-04-24 - Rocardp Vazquez Gomez (MR !274)  
   declare event types 27163491, 27163291, 27163282, 27163681 obsolete  
   add new decfiles:  
   + 27163490 : Ds2317_Dspi0,KKpi=DecProdCut.dec  
   + 27163496 : Ds2317_Dspi0,KKpi=TightCut.dec  
   + 27163290 : Ds2317_Dsstgamma,Dsgamma,KKpi=DecProdCut.dec  
   + 27163292 : Ds2317_Dsstgamma,Dsgamma,KKpi=TightCut.dec  
   + 27163280 : Ds2460_Dsstgamma,Dsgamma,KKpi=DecProdCut.dec  
   + 27163283 : Ds2460_Dsstgamma,Dsgamma,KKpi=TightCut.dec  
   + 27163680 : Ds2460_Dsstpi0,Dsgamma,KKpi=DecProdCut.dec  
   + 27163683 : Ds2460_Dsstpi0,Dsgamma,KKpi=TightCut.dec  
  
! 2019-03-08 - Yanxi Zhang (MR !249)  
   Add a decfile of Bc+ decay  
   + 14195070 : Bc_D0D0Pi=BcVegPy,DecProdCut.dec  
  
! 2019-03-21 - Roberta Cardinale (MR !256)  
   Added new decay files for hc studies    
   + 28144040 : hc_etacmumu,pp,aschic1=DecProdCut.dec    
	 + 28146040 : hc_etacmumu,phiphi,aschic1=DecProdCut.dec    
	 + 28144043 : chic1_Jpsimumu,pp=DecProdCut.dec    
  
! 2019-03-21 - Santiago Gomez Arias (MR !255)  
   Added new decfiles:  
   + 14543022 : Bc_Jpsimunu,pp=BcVegPy,DecProdCut.dec  
   + 14543021 : Bc_etacmunu,pp=BcVegPy,DecProdCut.dec  
  
! 2019-04-05 - Yiming Li (MR !260)  
   Added 2 decay files for Bc -> B* K* decays  
   + 14145210: Bc_BstKst,JpsiKgamma,Kpi=BcVegPy,DecProdCut.dec  
   + 14165220: Bc_BstKst,Dpigamma,Kpi=BcVegPy,DecProdCut.dec  
  
! 2019-03-07 - George Lovell (!MR 248)  
   Added one new decfile, an updated version of 12267140, with more realistic proportions for resonances (with requested fixes)  
   + 12267150 : Bu_D0Kpipi,KSpipi=addResTuned,TightCut,PHSP,update.dec  
  
! 2019-04-09 - Svende Braun (MR !241)  
   updated 4 previously unused dec files to include correct tight generator level cuts  
   + 11443022 : Bd_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 12445022 : Bu_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 13444022 : Bs_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 15144062 : Lb_JpsiKp,mumu,PPTcuts=TightCut.dec  
  
! 2019-04-08 - Wenqian Huang (MR !263)  
   Add new decfiles:  
   + 14543201 : Bc_chic1mu,mm=DecProdCut.dec  
   + 14743201 : Bc_chic2mu,mm=DecProdCut.dec  
   + 11443010 : Bd_Jpsipipipi,mm,inclusive=DecProdCut.dec  
   + 12445401 : Bu_Jpsipipipi,mm,inclusive=DecProdCut.dec   
  
! 2019-04-14 - Adam Morris (MR !267)  
   Change one branching fraction in 2 similar cocktails:  
   + 11996412 : Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut.dec  
   + 11996413 : Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut,TightCut.dec  
  
! 2019-04-23 - Roberta Cardinale (MR !273)  
   added 8 decfiles  
   + 12135020 : Bu_etacpi+rho0,pp=DecProdCut.dec  
   + 11134420 : Bd_etacpi+rho-,pp=DecProdCut.dec  
   + 12135040 : Bu_etacK+rho0,pp=DecProdCut.dec  
   + 11134440 : Bd_etacK+rho-,pp=DecProdCut.dec  
   + 12135010 : Bu_JpsiK+rho0,pp=DecProdCut.dec  
   + 11134410 : Bd_JpsiK+rho-,pp=DecProdCut.dec  
   + 12135030 : Bu_Jpsipi+rho0,pp=DecProdCut.dec  
   + 11134430 : Bd_Jpsipi+rho-,pp=DecProdCut.dec  
  
! 2019-04-09 - Nis Meinert (MR !256)  
   add new decfiles  
   + 15166130 : Lb_D0Lambda,K3pippi=phsp,DecProdCut_pCut1600MeV.dec  
   + 16166140 : Xib0_D0Lambda,K3pippi=phsp,DecProdCut_pCut1600MeV.dec  
   + 16164140 : Xib0_D0Lambda,Kpippi=DecProdCut_pCut1600MeV.dec  
  
! 2019-03-15 - Michael Wilkinson (MR !253)  
   add new decfiles for Xi_c0 BR study:  
   + 25203000 : Lc_pKpi-res=LHCbAcceptance.dec  
   + 26264081 : Xic0_Lcpi,pKpi-res=LHCbAcceptance.dec  
  
! 2019-04-16 - Sebastian Schulte (MR !270)  
   add new decfile  
   + 11522012 : Bd_pienu=DecProdCut,M4.5GeV,EvtGenCut.dec  
  
! 2019-03-06 - Marco Pappagallo (MR !247)  
   add new decfiles  
   + 27375261 : Ds1_Dsstmumu,KKpi=TightCut.dec  
   + 27375281 : Ds2460_Dsstmumu,KKpi=TightCut.dec  
  
! 2019-04-26 - Lorenz Capriotti (MR !275)  
   Add 2 decfiles:  
   + 12145402 : Bu_KOmegaJpsi,pi0pi+pi-=PHSP,mm=TightCut2.dec  
   + 12145403 : Bu_JpsiKpipipi0=PHSP,mm=TightCut.dec  
  
! 2019-04-18 - Xuhao Yuan (MR !272)  
   add 2 new decfiles:  
   + 14543020 : Bc_psi2SMuNu,mm=BcVegPy,ffEbert,DecProdCut.dec  
   + 14543023 : Bc_psi2SMuNu,mm=BcVegPy,ffKiselev,DecProdCut.dec  
  
! 2019-04-17 - George Lovell (MR !271)  
   Add 10 decfiles for B2DKPiPi backgrounds:  
   + 12267300 : Bu_Dst0Kpipi,D0gamma,KSpipi=TightCut,PHSP.dec  
   + 12197150 : Bu_D0Ds,KSpipi,KKpi=TightCut,Ddalitz.dec  
   + 12267500 : Bu_Dst0Kpipi,D0pi0,KSpipi=TightCut,PHSP.dec  
   + 12267510 : Bu_Dst0pipipi,D0pi0,KSpipi=TightCut,PHSP.dec  
   + 11268100 : Bd_Dst-Kpipi,D0pi-,KSpipi=TightCut,PHSP.dec  
   + 13168110 : Bs_D0Kpipipi,KSpipi=TightCut,PHSP.dec  
   + 12167100 : Bu_D0a1,KSpipi,KKpi=TightCut,PHSP.dec  
   + 12267310 : Bu_Dst0pipipi,D0gamma,KSpipi=TightCut,PHSP.dec  
   + 11268110 : Bd_Dst-pipipi,D0pi-,KSpipi=TightCut,PHSP.dec  
   + 11168110 : Bd_D0pipipipi,KSpipi=TightCut,PHSP.dec  
  
! 2019-04-12 - Miguel Ramos Pernas (MR !268)  
   Update decfiles:  
   + 34512120 : Ks_pimunu,m=TightCut.dec  
   + 34512108 : Ks_pimunu=TightCut.dec  
   
! 2019-02-22 - Elisabeth Maria Niel (MR !232)  
   Add decfiles:  
   + 34000000 : minbias=BiasedKsPt350MeV.dec  
   + 33000000 : minbias=BiasedLambdaPt300MeV.dec  
  
! 2019-04-05 - Antonio Romero Vidal (MR !261, !262)  
   Add decfiles:  
   + 11496400 : Bd_D0XcIncl,Kpi,3piIncl=TightCut.dec  
   + 13496400 : Bs_D0XcIncl,Kpi,3piIncl=TightCut.dec  
   + 12495410 : Bu_D0XcIncl,Kpi,3piIncl=TightCut.dec  
   + 11496402 : Bd_DpXcIncl,Kpi,3piIncl=TightCut.dec  
   + 13496402 : Bs_DpXcIncl,Kpi,3piIncl=TightCut.dec  
   + 12497402 : Bu_DpXcIncl,Kpi,3piIncl=TightCut.dec  
  
! 2019-03-19 - Mengzhen Wang (MR !254)  
   Add 1 new decfiles  
   + 15144059 : Lb_JpsipK,mm=XLL2,DecProdCut.dec  
  
! 2019-04-26 Vanya Belyaev (MR !276)  
   new decfiles  
   + 16166014  Lbstar6150_Lbpipi,Lcpi=TightCut  
  

