DecFiles v30r65 2021-10-05 
==========================  
 
! 2021-10-05 - Michal Kreps (MR !845)  
   Remove event type 15146104 from table_obsolete.sql as decay file is not removed and does not have to be removed  
  
  
