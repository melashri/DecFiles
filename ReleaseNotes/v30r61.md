DecFiles v30r61 2021-07-22 
==========================  
 
! 2021-07-22 - Mark Smith (MR !797)  
   Add new decay file  
   + 12875004 : Bu_Dststmunu,Dstpi=CocktailHigher,TightCut  
  
! 2021-07-21 - William Barter (MR !796)  
   Add 4 new decay files  
   + 49000092 : dijet=b,m110GeV,mu_tight  
   + 49000093 : dijet=b,m140GeV,mu_tight  
   + 49000091 : dijet=b,m70,110GeV,mu_tight  
   + 49000090 : dijet=b,m70GeV,mu_tight  
  
! 2021-07-19 - Asier Pereiro Castro (MR !794)  
   Add new decay file  
   + 13104005 : Bs_Kst0Kst0=tightCut,HelAmpsFromData  
  
! 2021-07-19 - Vitalii Lisovskyi (MR !793)  
   Add 7 new decay files  
   + 12115016 : Bu_K2mu2e=TightCut  
   + 12115015 : Bu_K4mu=TightCut  
   + 12117015 : Bu_K6mu=TightCut  
   + 39114032 : eta_2mu2e=TightCut  
   + 39114031 : eta_4mu=TightCut  
   + 39114002 : incl_phi,2mu2e=TightCut  
   + 39114001 : incl_phi,4mu=TightCut  
  
! 2021-07-19 - Zhanwen Zhu (MR !792)  
   Add 5 new decay files  
   + 15104013 : Lb_pKKK_PHSP=DecProdCut  
   + 15104012 : Lb_pKKpi_PHSP=DecProdCut  
   + 16104042 : Xib0_pKKK_PHSP=DecProdCut  
   + 16104041 : Xib0_pKKpi_PHSP=DecProdCut  
   + 16104040 : Xib0_pKpipi_PHSP=DecProdCut  
  
! 2021-07-15 - Yasmine Sara Amhis (MR !791)  
   Add 2 new decay files  
   + 15124401 : Lb_pKstee,Kpi0=phsp,DecProdCut  
   + 15114401 : Lb_pKstmm,Kpi0=phsp,DecProdCut  
  
! 2021-07-15 - Yasmine Sara Amhis (MR !790)  
   Add new decay file  
   + 15124211 : Lb_EtappK,e+e-g=DecProdCut  
  
! 2021-07-15 - Yu Lu (MR !789)  
   Add 3 new decay files  
   + 12197007 : Bu_DsLcp,KKpi,pKPi=PHSP,DecProdCut  
   + 12197006 : Bu_LcLcPi,pKpi,pKPi=PHSP,DecProdCut  
   + 12197004 : Bu_LcXicPi,pKpi,pKPi=PHSP,DecProdCut  
  
! 2021-07-14 - Basem Khanji (MR !788)  
   Add new decay file  
   + 13512030 : Bs_Ktaunu,mununu=DecProdCut  
  
! 2021-07-12 - Huanhuan Liu (MR !787)  
   Add 6 new decay files  
   + 11198053 : Bd_DstD0K,D0pi_K3pi,Kpi=sqDalitz21,DecProdCut  
   + 11198054 : Bd_DstD0K,D0pi_Kpi,K3pi=sqDalitz21,DecProdCut  
   + 12199051 : Bu_Dst+D-K,K2pi,D0pi_K3pi=sqDalitz21,DecProdCut  
   + 12297055 : Bu_Dst+D-K,K2pi,D0pi_Kpi=sqDalitz21,DecProdCut  
   + 12199052 : Bu_Dst-D+K,K2pi,D0pi_K3pi=sqDalitz21,DecProdCut  
   + 12297056 : Bu_Dst-D+K,K2pi,D0pi_Kpi=sqDalitz21,DecProdCut  
   Modify 6 decay files  
   + 11198093 : Bd_DstD0K,D0pi_K3pi,Kpi=sqDalitz23,DecProdCut  
   + 11198094 : Bd_DstD0K,D0pi_Kpi,K3pi=sqDalitz23,DecProdCut  
   + 12199091 : Bu_Dst+D-K,K2pi,D0pi_K3pi=sqDalitz23,DecProdCut  
   + 12297095 : Bu_Dst+D-K,K2pi,D0pi_Kpi=sqDalitz23,DecProdCut  
   + 12199092 : Bu_Dst-D+K,K2pi,D0pi_K3pi=sqDalitz23,DecProdCut  
   + 12297096 : Bu_Dst-D+K,K2pi,D0pi_Kpi=sqDalitz23,DecProdCut  
  
! 2021-07-05 - Jessy Daniel (MR !780)  
   Add 6 new decay files  
   + 12165597 : Bu_D0Kst-,KSpipipi0,Kpi0=TightCut,PHSP  
   + 12165512 : Bu_D0rho-,KSpipipi0=TightCut,PHSP  
   + 12165740 : Bu_Dst0Kst-,D0gamma,KSpipipi0=TightCut,PHSP  
   + 12165544 : Bu_Dst0Kst-,D0pi0,KSpipipi0=TightCut,PHSP  
   + 12165720 : Bu_Dst0Rho-,D0gamma,KSpipipi0=TightCut,PHSP  
   + 12165570 : Bu_Dst0Rho-,D0pi0,KSpipipi0=TightCut,PHSP  
  
! 2021-06-28 - Tianwen Zhou (MR !777)  
   Add 4 new decay files  
   + 15198132 : Lb_Xic3080+D-,D+Lambda,KPiPi,KPiPi,PPi=TightCut  
   + 15196101 : Lb_Xic3080D0,D0Lambda,KPi,KPi,PPi=TightCut  
   + 16166141 : Xib0_Xic3080+pi-,D+Lambda0,Kpipi,ppi=phsp,TightCut  
   + 16165135 : Xib_Xic3080pi,D0Lambda0,Kpi,ppi=phsp,TightCut  
  
