
!========================= DecFiles v30r37 2019-10-08 =======================  
  
! 2019-10-07 - Nicola Anne Skidmore (MR !359)  
   Add new decay file  
   + 15198002 : Lb_ScppDK,Lcpi,Kpipi=DecProdCut  
  
! 2019-09-27 - Daniel Patrick O'Hanlon (MR !357)  
   Add new decay file  
   + 15164531 : Lb_D-p+pi0,KSpi-=DecProdCut  
  
! 2019-09-26 - Hongjie Mu (MR !356)  
   Add new decay file  
   + 39112230 : eta_mumugamma=TightCut  
   Modify create_options.py to move part of the event type space from phi to eta.  
  
! 2019-09-23 - Thomas Henry Hancock (MR !355)  
   Add 2 new decay files  
   + 13174024 : Bs_D0Jpsi,Kpi,mm=DecProdCut  
   + 12275079 : Bu_DsJpsi,KKpi,mm=DecProdCut  
  
! 2019-08-20 - Michel De Cian (MR !345)  
   Add new decay file  
   + 37103002 : K+_pipipi=TightCut,TwoPionsInAcceptance  
  
